#! /bin/bash

for directory in qor-core qor-kernel qor-riscv
do
    pushd $directory
    cargo fmt
    popd
done
