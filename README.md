# Qor

Qor is a simple hobby operating system written in Rust for RISC-V. A previous version can be found on [Github](https://www.github.com/AshTS/Qor). The previous versions targeted QEMU's virt device. This new version will target the Vision Five 2 made by StarFive. [This blog post](https://zyedidia.github.io/blog/posts/2-baremetal-visionfive/) by Zachary Yedidia is the source for the methods used to flash custom firmware to the VisionFive 2.

Technical Documentation: https://doc-en.rvspace.org/JH7110/PDF/JH7110_TRM_StarFive_Preliminary_V2.pdf

Device Tree: https://github.com/starfive-tech/linux/blob/fe02cda194f56c0cc6158d7bdab5cbf376bfe689/arch/riscv/boot/dts/starfive/jh7110-common.dtsi

# Running On Hardware

This process is adapted from [this blog post](https://zyedidia.github.io/blog/posts/2-baremetal-visionfive/) by Zachary Yedidia.

Writing the Qor binary to the VisionFive 2 involves supplying our binary as the firmware while undergoing the firmware update process. As such, this process overwrites the existing firmware, and returning to using the VisionFive 2 to run a Linux image will require replacing the original firmware. The VisionFive [quick start guide](https://doc-en.rvspace.org/VisionFive2/Quick_Start_Guide) includes instructions on how to do this.

Our chosen method for writing the new firmware is over UART. When the boot-mode switches are switched to UART mode, upon restart, the board will request the recovery binary over XMODEM. This file is supplied by StarFive, and is available [here](https://github.com/starfive-tech/Tools/blob/bc6dc7e33e0c2466db0476b5043f0f77842f98f0/recovery/jh7110-recovery-20221205.bin). Once this file is sent and verified, we are presented with a menu, we want to select `update fw_verif/uboot in flash`, at time of writing, this is option 2. After selecting this option, we send the image produced by the `build-image.sh` script over XMODEM. This will write the image to the flash on the board. Finally, when the boot-mode switches are returned to Flash mode and the board is reset, our image will be loaded.