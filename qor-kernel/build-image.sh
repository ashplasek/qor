#! /bin/bash

llvm-objcopy -O binary target/riscv64gc-unknown-none-elf/$1/qor-kernel raw.bin
~/vf2-bin/vf2-imager -i raw.bin -o kernel.img
rm raw.bin