use qor_core::{
    memory::{
        allocators::{bitmap::PageBitmapAllocator, bump::AllocationError},
        MemoryUnit,
    },
    sync::OnceSyncCell,
};
use qor_riscv::memory::page::Page;

use crate::memory::page::bump::PAGE_BUMP_ALLOCATOR;

/// Global page grained bitmap allocator.
pub static PAGE_BITMAP_ALLOCATOR: OnceSyncCell<PageBitmapAllocator<Page>> = OnceSyncCell::new();

/// Get a reference to the global page bitmap allocator if it exists, otherwise panic with an appropriate error.
///
/// # Panics
///
/// This function will panic if the `PAGE_BITMAP_ALLOCATOR` is not yet initialized.
#[allow(dead_code)]
pub fn get_page_bitmap_allocator() -> &'static PageBitmapAllocator<Page> {
    PAGE_BITMAP_ALLOCATOR
        .try_get()
        .expect("Page bitmap allocator not yet initialized")
}

/// Initialize the global page bitmap allocator with a certain amount of memory from the global page bump allocator.
///
/// # Errors
///
/// This function returns an error if it was not possible to allocate `memory_amount` pages from the
/// `PAGE_BUMP_ALLOCATOR`.
///
/// # Panics
///
/// This function will panic if the `PAGE_BITMAP_ALLOCATOR` is already initialized.
pub fn initialize_page_bitmap_allocator(
    memory_amount: MemoryUnit<{ crate::specific_arch::PAGE_SIZE }>,
) -> Result<(), AllocationError> {
    let alloted_memory = PAGE_BUMP_ALLOCATOR.allocate(memory_amount.raw())?;
    let bitmap_allocator = PageBitmapAllocator::from_pages(alloted_memory);

    PAGE_BITMAP_ALLOCATOR
        .try_initialize(bitmap_allocator)
        .ok()
        .expect("Page bitmap allocator already initialized");

    info!(
        "Initialized global page grained bitmap allocator with {} of memory",
        memory_amount
    );
    Ok(())
}
