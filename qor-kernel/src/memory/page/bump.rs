use qor_core::memory::allocators::bump::PageBumpAllocator;
use qor_riscv::memory::page::{Page, PageAllocator};

use crate::memory::AllocatorInitializationError;

/// Global page grained bump allocator.
pub static PAGE_BUMP_ALLOCATOR: PageBumpAllocator<Page> = PageBumpAllocator::new();

/// Initialize the global page grained bump allocator.
///
/// # Errors
///
/// This function will return an error under five conditions:
/// - The `HEAP_START` pointer is null.
/// - The `HEAP_END` pointer is null.
/// - The `HEAP_START` pointer is unaligned.
/// - The `HEAP_END` pointer is unaligned.
/// - The `HEAP_START` and `HEAP_END` pointers are misordered.
pub fn initialize_page_bump_allocator() -> Result<(), AllocatorInitializationError> {
    // Access the heap start and end variables to store them locally
    // Safety: These are just constants being read
    let heap_start = unsafe { crate::specific_arch::asm::HEAP_START };
    let heap_end: *mut Page = unsafe { crate::specific_arch::asm::HEAP_END };

    // Manually check the safety requirements of the `assign_region` function.
    if heap_start.is_null() {
        error!("Heap start is a null pointer.");
        return Err(AllocatorInitializationError);
    } else if heap_end.is_null() {
        error!("Heap end is a null pointer.");
        return Err(AllocatorInitializationError);
    } else if !heap_start.is_aligned() {
        error!("Heap start is unaligned: {:x?}.", heap_start);
        return Err(AllocatorInitializationError);
    } else if !heap_end.is_aligned() {
        error!("Heap end is unaligned: {:x?}.", heap_end);
        return Err(AllocatorInitializationError);
    } else if heap_start >= heap_end {
        error!("Heap start and end are in the wrong order (start: {:x?}, end: {:x?}), unable to initialize bump allocator.", heap_start, heap_end);
        return Err(AllocatorInitializationError);
    }

    // Safety:
    // Non-null, aligned, and proper ordering are checked manually by the above code.
    // Valid range is given by the definition of these symbols in the linker script, and them only being used here.
    unsafe {
        PAGE_BUMP_ALLOCATOR.assign_region(
            crate::specific_arch::asm::HEAP_START..crate::specific_arch::asm::HEAP_END,
        );
    }

    info!("Initialized global page grained bump allocator.");

    Ok(())
}

#[derive(Debug, Clone, Copy)]
pub struct PageBumpAllocatorMarker;

impl PageAllocator for PageBumpAllocatorMarker {
    fn alloc<T: Sized>(object: T) -> &'static mut T {
        PAGE_BUMP_ALLOCATOR
            .allocate_object(object)
            .expect("Unable to allocate object via `PageBumpAllocatorMarker`")
    }

    fn free<T: Sized>(_mem: &'static mut T) {
        warn!("The page bump allocator does not free pages.");
    }
}
