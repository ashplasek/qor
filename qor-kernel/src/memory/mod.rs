pub mod byte;
pub mod page;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AllocatorInitializationError;
