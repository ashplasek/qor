use core::fmt::Write;

/// Required panic handler for the kernel
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    let _ = write!(
        unsafe {
            crate::drivers::uart::UART_DRIVER
                .try_get()
                .unwrap()
                .force_access()
        },
        "Panic!\r\n{info:?}"
    );

    loop {}
}
