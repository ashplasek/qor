// Required Features
#![feature(fn_align)]
// Required to guarantee 4 byte alignment for functions to be called via interrupt returns

// Use the _start symbol instead of main
#![no_main]
// Do not link against a standard library (the core and alloc crates will handle anything we would do with the standard
// library)
#![no_std]

#[macro_use]
extern crate qor_core;

pub mod arch;
pub mod drivers;
pub mod kprint;
pub mod memory;
pub mod panic;

use arch::riscv::visionfive2::arch as specific_arch;
use core::hint::spin_loop;

/// Kernel Initialize Function (Called immediately after boot)
///
/// # Panics
///
/// This function will panic if any necessary steps for booting are unable to be completed.
#[no_mangle]
#[repr(align(4))]
pub extern "C" fn kinit() {
    // Set up the global UART driver and set it as the global driver
    let uart = specific_arch::default_uart_driver();
    drivers::uart::set_global(uart);

    // Use the global UART driver as the backing for logging.
    kprint::assign_uart_logger();

    // Initialize a bump allocator of pages
    memory::page::bump::initialize_page_bump_allocator()
        .expect("Unable to initialize bump allocator");

    // Initialize the global page grained bitmap allocator
    let dynamic_page_allocation_size = qor_core::memory::MiByteCount::new(1024);
    memory::page::bitmap::initialize_page_bitmap_allocator(dynamic_page_allocation_size.convert())
        .expect("Unable to initialize bitmap allocator");

    // Initialize the global byte grained allocator
    let dynamic_byte_allocation_size = qor_core::memory::MiByteCount::new(128);
    memory::byte::initialize_global_byte_allocator(dynamic_byte_allocation_size.convert());

    // Initialize the page table
    let page_table = memory::page::bump::PAGE_BUMP_ALLOCATOR
        .allocate_object(specific_arch::KernelPageTable::new())
        .unwrap();

    // Identity map kernel memory
    specific_arch::identity_map_kernel(page_table);

    trace!("Returning from `kinit`");
    loop {
        spin_loop();
    }
}
