pub mod arch {
    use qor_riscv::{
        drivers::uart::{Initialized, UARTDriver},
        memory::{
            mmu::{table::MAPPING_1GIB, PhysicalAddress},
            page::PageAllocator,
        },
    };

    pub use qor_riscv::asm;
    pub use qor_riscv::memory::mmu::table::PageTable;

    use crate::memory::page::bump::PageBumpAllocatorMarker;

    pub type UARTDriverType = UARTDriver<Initialized>;
    pub type KernelPageTable = PageTable<MAPPING_1GIB, PageBumpAllocatorMarker>;

    pub const PAGE_SIZE: usize = 4096;

    #[must_use]
    pub fn default_uart_driver() -> UARTDriverType {
        let uart_mmio = qor_core::drivers::mmio::MMIOInterface::new(0x1000_0000);
        // Safety: The interface passed points to a valid base address of a memory mapped NS16550a device, as the
        // address 0x1000_0000 comes from the device tree.
        let uart = unsafe { qor_riscv::drivers::uart::UARTDriver::new(uart_mmio) };

        let uart_data_length = qor_core::drivers::uart::DataLength::EightBits;
        let uart_interrupts = qor_core::drivers::uart::Interrupts::receive();
        let uart_baud_rate = 115_200;
        let uart_data_mode = qor_core::drivers::uart::DataMode::new(
            false,
            false,
            uart_data_length,
            qor_core::drivers::uart::Parity::None,
        );

        uart.initialize(uart_data_mode, uart_interrupts, uart_baud_rate)
    }

    macro_rules! id_map {
        ($page_table: ident, $start: expr, $end: expr, $mode: ident) => {
            unsafe {
                $page_table.add_identity_mapping(
                    PhysicalAddress::panicking_new($start),
                    PhysicalAddress::panicking_new($end),
                    qor_riscv::memory::mmu::MemoryProtectionFlags::$mode,
                    false,
                );
            }
        };
    }

    /// Initialize a `PageTable` to identity map the kernel.
    ///
    /// The locations used here are referenced from the memory map in
    /// <https://doc-en.rvspace.org/JH7110/PDF/JH7110_TRM_StarFive_Preliminary_V2.pdf>
    pub fn identity_map_kernel<Allocator: PageAllocator>(
        page_table: &mut PageTable<MAPPING_1GIB, Allocator>,
    ) {
        id_map!(page_table, 0x00_1000_0000, 0x00_120e_ffff, ReadWrite);
        id_map!(page_table, 0x00_1300_0000, 0x00_130e_ffff, ReadWrite);
        id_map!(page_table, 0x00_1500_0000, 0x00_1570_ffff, ReadWrite);
        id_map!(page_table, 0x00_1600_0000, 0x00_1705_ffff, ReadWrite);
        id_map!(page_table, 0x00_170c_0000, 0x00_170c_ffff, ReadWrite);
        id_map!(page_table, 0x00_1800_0000, 0x00_180f_ffff, ReadWrite);
        id_map!(page_table, 0x00_1980_0000, 0x00_1984_ffff, ReadWrite);
        id_map!(page_table, 0x00_1987_0000, 0x00_1989_ffff, ReadWrite);
        id_map!(page_table, 0x00_2000_8000, 0x00_2003_7fff, ReadWriteExecute);
        id_map!(page_table, 0x00_2100_8000, 0x00_28ff_ffff, ReadWriteExecute);
        id_map!(page_table, 0x00_2940_0000, 0x00_294f_ffff, ReadWrite);
        id_map!(page_table, 0x00_2959_0000, 0x00_2959_ffff, ReadWrite);
        id_map!(page_table, 0x00_295b_0000, 0x00_295e_ffff, ReadWrite);
        id_map!(page_table, 0x00_2a00_0000, 0x00_2a00_81ff, ReadExecute);
        id_map!(page_table, 0x00_2b00_0000, 0x00_2cff_ffff, ReadWrite);
        id_map!(page_table, 0x00_3000_0000, 0x00_3fff_ffff, ReadWrite);
        id_map!(page_table, 0x00_4000_0000, 0x02_3fff_ffff, ReadWriteExecute);
        id_map!(page_table, 0x04_4000_0000, 0x06_3fff_ffff, ReadWriteExecute);
        id_map!(page_table, 0x09_0000_0000, 0x09_cfff_ffff, ReadWrite);

        info!("Identity mapped kernel");
    }
}
