use qor_core::{
    drivers::uart::GlobalUART,
    sync::{MutexGuard, OnceSyncCell},
};

use crate::specific_arch::UARTDriverType;

pub static UART_DRIVER: OnceSyncCell<GlobalUART<UARTDriverType>> = OnceSyncCell::new();

/// Set a `UARTDriver` as the `GlobalUART` driver.
///
/// # Panics
///
/// This function will panic if the global UART driver has already been initialized.
pub fn set_global(driver: UARTDriverType) {
    UART_DRIVER
        .try_initialize(GlobalUART::new(driver))
        .ok()
        .expect("Unable to initialize global UART driver");
}

/// Get a safe reference to the `GlobalUART` driver.
///
/// # Panics
///
/// This function will panic if the `GlobalUART` driver has not been set.
pub fn get_global<'a>() -> Option<MutexGuard<'a, UARTDriverType>> {
    UART_DRIVER
        .try_get()
        .expect("Global UART driver has not been set")
        .attempt_lock()
}

/// Get a safe reference to the `GlobalUART` driver, spinning until the lock on the driver has been released
///
/// # Panics
///
/// This function will panic if the `GlobalUART` driver has not been set.
pub fn spin_get_global<'a>() -> MutexGuard<'a, UARTDriverType> {
    UART_DRIVER
        .try_get()
        .expect("Global UART driver has not been set")
        .spin_lock()
}
