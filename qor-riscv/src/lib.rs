#![no_std]
#![feature(strict_provenance)] // Required for pointers to be treated as addresses

pub mod asm;
pub mod drivers;
pub mod memory;
