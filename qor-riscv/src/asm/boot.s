# Do not produce compressed instructions
.option norvc

# Section which will be placed as 0x4000_0000
.section .text.init
.global _start
_start:
    csrr t0, mhartid
    li t1, 1
    bne t0, t1, _hlt # branch to _hlt if mhartid != 1
    # Clear the BSS section by writing 8 byte double words to it

    # Load the start and end pointers
    la a0, _bss_start
    la a1, _bss_end

    # If the bss section is empty
    bgeu a0, a1, _start_init

_start_zero_bss_loop:
    # Store double word
    sd zero, (a0)
    # Increment by 8
    addi a0, a0, 8
    # Jump back
    bltu a0, a1, _start_zero_bss_loop

_start_init:
    # Initialize the stack pointer
    la sp, _stack_end
    
    # Set up the machine status register
    li t0, 0b11 << 11 | (1 << 7) | (1 << 3)
    csrw mstatus, t0

    # Set the mret address to kinit
    la t1, kinit
    csrw mepc, t1

    # We do not set the trap vector, as we are given control with a functioning exception handler.
    # la t2, _hlt
    # csrw mtvec, t2

    # Make sure no interrupts occur during initialization
    csrw mie, zero

    # Set up the return address for when kinit returns
    la ra, _hlt

    # Call kinit
    mret
_hlt:
    wfi
    j _hlt