use core::arch::global_asm;

global_asm!(include_str!("boot.s"));
global_asm!(include_str!("mem.s"));

// Values defined in assembly which now need to be brought into rust
extern "C" {
    pub static HEAP_START: *mut crate::memory::page::Page;
    pub static HEAP_END: *mut crate::memory::page::Page;
}
