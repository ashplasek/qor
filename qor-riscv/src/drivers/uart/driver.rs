use core::marker::PhantomData;

use qor_core::drivers::uart::{DataMode, Interrupts, UARTInterface};

use super::raw;
use qor_core::drivers::mmio::MMIOInterface;

const UART_CLOCK: u32 = 22_729_000;

/// UART Driver for a memory mapped NS16550a device.
///
/// # Safety
///
/// - For a `UARTDriver` object to exist, it must contain an `MMIOInterface` which points to a valid base address of a
///     memory mapped NS16550a device.
#[derive(Clone, Copy)]
#[allow(clippy::module_name_repetitions)]
pub struct UARTDriver<State> {
    interface: MMIOInterface,
    _marker: PhantomData<State>,
}

impl UARTDriver<Uninitialized> {
    /// Construct a new `UARTDriver` in an uninitialized state.
    ///
    /// # Safety
    ///
    /// - The `interface` passed must point to a valid base address of a memory mapped NS16550a device.
    #[must_use]
    pub const unsafe fn new(interface: MMIOInterface) -> Self {
        Self {
            interface,
            _marker: PhantomData,
        }
    }

    /// Initialize the `UARTDriver` for the use of a particular `DataMode`, `Interrupts` status, and `baud_rate`.
    #[must_use]
    pub fn initialize(
        self,
        data: DataMode,
        interrupts: Interrupts,
        baud_rate: u32,
    ) -> UARTDriver<Initialized> {
        let lcr = data.compute_lcr();

        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr);
        }

        // Enable the FIFO
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_fifo_control_register(&self.interface, 1);
        }

        // Set the interrupt state
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_interrupt_enable_register(&self.interface, interrupts.value());
        }

        // Compute the divisor for clock frequency
        let clock_divisor = raw::divisor_from_baud(UART_CLOCK, baud_rate);
        let clock_divisor_bytes = clock_divisor.to_le_bytes();

        // Set the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr | (1 << 7));
        }

        // Write the clock divisor
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants. Additionally, the DLAB bit (bit 7) of the LCR register has
        // been set.
        unsafe {
            // TODO: Return these to the default state!
            raw::set_divisor_latch_ls_register(&self.interface, clock_divisor_bytes[0]);
            raw::set_divisor_latch_ms_register(&self.interface, clock_divisor_bytes[1]);
        }

        // Clear the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr);
        }

        UARTDriver {
            interface: self.interface,
            _marker: PhantomData,
        }
    }
}

impl UARTDriver<Initialized> {
    /// Set the `DataMode`
    pub fn set_data_mode(&self, data: DataMode) {
        let lcr = data.compute_lcr();

        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr);
        }
    }

    /// Set the `Interrupts` state
    pub fn set_interrupt_enables(&self, interrupts: Interrupts) {
        // Set the interrupt state
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_interrupt_enable_register(&self.interface, interrupts.value());
        }
    }

    /// Set the `baud_rate` (requires updating the `DataMode` as well)
    pub fn set_baud_rate(&self, data: DataMode, baud_rate: u32) {
        let lcr = data.compute_lcr();

        // Compute the divisor for clock frequency
        let clock_divisor = raw::divisor_from_baud(UART_CLOCK, baud_rate);
        let clock_divisor_bytes = clock_divisor.to_le_bytes();

        // Set the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr | (1 << 7));
        }

        // Write the clock divisor
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants. Additionally, the DLAB bit (bit 7) of the LCR register has
        // been set.
        unsafe {
            raw::set_divisor_latch_ls_register(&self.interface, clock_divisor_bytes[0]);
            raw::set_divisor_latch_ms_register(&self.interface, clock_divisor_bytes[1]);
        }

        // Clear the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr);
        }
    }

    /// Send a byte over the uart port
    pub fn send_byte_sync(&self, byte: u8) {
        // Wait until the transmitter holding register status bit is set.
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        while (unsafe { raw::read_line_status_register(&self.interface) } & (1 << 5)) == 0 {}

        // Send the byte
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants. Additionally, the DLAB bit is cleared.
        unsafe {
            raw::set_transmitter_holding_register(&self.interface, byte);
        }
    }

    /// Attempt to receive a byte from the uart port
    #[must_use]
    pub fn receive_byte(&self) -> Option<u8> {
        // Read the line status register
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        let line_status_register = unsafe { raw::read_line_status_register(&self.interface) };

        // If the data ready bit is set, read a byte, otherwise return `None`
        if line_status_register & 1 > 0 {
            // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
            // guarantee from `UARTDriver` safety invariants. Additionally, the DLAB bit is cleared.
            Some(unsafe { raw::read_receiver_buffer_register(&self.interface) })
        } else {
            None
        }
    }

    #[must_use]
    pub fn read_speed_divisor(&self) -> u16 {
        // Set the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        let lcr = unsafe { raw::read_line_control_register(&self.interface) };

        // Set the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr | (1 << 7));
        }

        // Write the clock divisor
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants. Additionally, the DLAB bit (bit 7) of the LCR register has
        // been set.
        let (low, high) = unsafe {
            let low = raw::read_divisor_latch_ls_register(&self.interface);
            let high = raw::read_divisor_latch_ms_register(&self.interface);

            (low, high)
        };

        // Clear the divisor latch
        // Safety: The `mmio` interface used points to a valid base address of a memory mapped NS16550a device by
        // guarantee from `UARTDriver` safety invariants.
        unsafe {
            raw::set_line_control_register(&self.interface, lcr);
        }

        (u16::from(high) << 8) | u16::from(low)
    }

    /// Construct a second handle pointing to a `UARTDriver`.
    ///
    /// # Safety
    ///
    /// Calling this function abandons any guarantees of exclusivity in UART operations.
    #[must_use]
    pub const unsafe fn duplicate(&self) -> Self {
        Self {
            _marker: PhantomData,
            interface: unsafe { self.interface.duplicate() },
        }
    }
}

impl UARTInterface for UARTDriver<Initialized> {
    fn set_data_mode(&self, data: DataMode) {
        self.set_data_mode(data);
    }

    fn set_interrupt_enables(&self, interrupts: Interrupts) {
        self.set_interrupt_enables(interrupts);
    }

    fn set_baud_rate(&self, data: DataMode, baud_rate: u32) {
        self.set_baud_rate(data, baud_rate);
    }

    fn send_byte_sync(&self, byte: u8) {
        self.send_byte_sync(byte);
    }

    fn receive_byte(&self) -> Option<u8> {
        self.receive_byte()
    }

    unsafe fn duplicate(&self) -> Self {
        self.duplicate()
    }
}

impl core::fmt::Write for &UARTDriver<Initialized> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for b in s.bytes() {
            self.send_byte_sync(b);
        }

        Ok(())
    }
}

pub struct Uninitialized;
pub struct Initialized;
