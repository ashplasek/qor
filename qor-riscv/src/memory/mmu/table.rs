use core::marker::PhantomData;

use atomic::Atomic;
use either::Either;
use qor_core::{debug, memory::MemoryUnit, trace};
use static_assertions::{assert_eq_align, assert_eq_size};

use crate::memory::page::{Page, PageAllocator};

use super::{MemoryFlags, MemoryProtectionFlags, PageTableEntry, PhysicalAddress, VirtualAddress};

pub const MAPPING_4KIB: usize = 4096;
pub const MAPPING_2MIB: usize = 2 * 1024 * 1024;
pub const MAPPING_1GIB: usize = 1024 * 1024 * 1024;

#[repr(C)]
#[allow(clippy::module_name_repetitions)]
#[repr(align(4096))]
/// Properly aligned array of [`PageTableEntry`]s with atomic access to serve as a [`PageTable`] for SV39 memory
/// management.
///
/// # Safety
pub struct PageTable<const MAPPING_SIZE: usize, Allocator: PageAllocator> {
    inner: [Atomic<PageTableEntry<MAPPING_SIZE>>; 512],
    marker: PhantomData<Allocator>,
}

assert_eq_size!(PageTable<MAPPING_4KIB, ()>, Page);
assert_eq_align!(PageTable<MAPPING_4KIB, ()>, Page);

impl<const MAPPING_SIZE: usize, Allocator: PageAllocator> PageTable<MAPPING_SIZE, Allocator> {
    /// Construct a new, empty, [`PageTable`]. The empty page table is filled with invalid entries.
    #[must_use]
    #[inline]
    pub const fn new() -> Self {
        // SAFETY: Every `PageTableEntry` inserted is defined to be `invalid`, and thus does not have its valid bit set,
        //     and therefore satisfies the safety invariants of `PageTable`.
        Self {
            inner: [const { Atomic::new(PageTableEntry::invalid()) }; 512],
            marker: PhantomData,
        }
    }

    /// Access the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get(&self, index: usize) -> Option<PageTableEntry<MAPPING_SIZE>> {
        self.inner
            .get(index)
            .map(|v| v.load(atomic::Ordering::SeqCst))
    }

    /// Set the `index`th [`PageTableEntry`] in the [`PageTable`]
    ///
    /// # Panics
    ///
    /// This function will panic if `index` is out of bounds (>=512).
    pub fn set(&self, index: usize, value: PageTableEntry<MAPPING_SIZE>) {
        assert!(index < 512);

        self.inner[index].store(value, atomic::Ordering::SeqCst);
    }

    /// Access the physical mapping within the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_physical_mapping(&self, index: usize) -> Option<PhysicalAddress<MAPPING_SIZE>> {
        self.get(index)?.mapping_pointer()
    }
}

impl<Allocator: PageAllocator> PageTable<MAPPING_4KIB, Allocator> {
    /// Map a virtual address to a physical address
    pub fn virtual_to_physical(
        &self,
        virtual_address: VirtualAddress<1>,
    ) -> Option<PhysicalAddress<1>> {
        let vpn_0 = virtual_address.virtual_page_number(0);

        self.get_physical_mapping(vpn_0).map(|mapping| {
            PhysicalAddress::const_new(mapping.inner() | virtual_address.page_offset(MAPPING_4KIB))
        })
    }

    /// Add a mapping from a virtual address to a physical address
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a page in memory.
    pub unsafe fn add_mapping(
        &self,
        virtual_address: VirtualAddress<MAPPING_4KIB>,
        physical_address: PhysicalAddress<MAPPING_4KIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        let vpn_0 = virtual_address.virtual_page_number(0);

        // Construct the new entry
        let entry = PageTableEntry::<MAPPING_4KIB>::construct_mapping(
            physical_address,
            permissions,
            user,
            false,
        );

        debug!(
            "Adding Mapping For 4KiB from {:#x} to {:#x} with permissions {:?} and user: {}",
            virtual_address.inner(),
            physical_address.inner(),
            permissions,
            user
        );

        self.set(vpn_0, entry);
    }

    /// Add a new range mapping
    ///
    /// # Safety
    ///
    /// The mapping constructed must be acceptable to the supervisor or userspace interacting with it.
    ///
    /// # Panics
    ///
    /// This function will panic if `length` is zero or the starting and ending `VirtualAddress`s are not stored in the
    /// same [`PageTable`]
    pub unsafe fn add_range_mapping(
        &self,
        start_physical: PhysicalAddress<MAPPING_4KIB>,
        start_virtual: VirtualAddress<MAPPING_4KIB>,
        length: MemoryUnit<MAPPING_4KIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        assert!(length.raw() > 0);
        let last_virtual = start_virtual.add(length.raw() - 1);
        assert_eq!(
            start_virtual.virtual_page_number(1),
            last_virtual.virtual_page_number(1)
        );

        for page_index in 0..length.raw() {
            self.add_mapping(
                start_virtual.add(page_index),
                start_physical.add(page_index),
                permissions,
                user,
            );
        }
    }
}

impl<Allocator: PageAllocator> PageTable<MAPPING_2MIB, Allocator> {
    /// Access the page table mapping within the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_table_mapping(
        &self,
        index: usize,
    ) -> Option<*mut PageTable<MAPPING_4KIB, Allocator>> {
        self.get(index)?.table_pointer()
    }

    /// Access the mapping within the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_mapping(
        &self,
        index: usize,
    ) -> Option<Either<PhysicalAddress<MAPPING_2MIB>, *mut PageTable<MAPPING_4KIB, Allocator>>>
    {
        let entry = self.get(index)?;
        if entry.valid() {
            if entry.flags() == Some(MemoryFlags::PointerToNextLevel) {
                Some(Either::Right(entry.table_pointer()?))
            } else {
                Some(Either::Left(entry.mapping_pointer()?))
            }
        } else {
            None
        }
    }

    /// Access the page table mapped in the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_table(&self, index: usize) -> Option<&PageTable<MAPPING_4KIB, Allocator>> {
        // SAFETY: The `PageTableEntry` safety invariants give that we have a valid pointer to a `PageTable`, and having
        //     a shared reference to this `PageTable` gives that no mutable references are taken out on the pointed to
        //     table.
        unsafe { self.get_table_mapping(index)?.as_ref() }
    }

    /// Map a virtual address to a physical address
    pub fn virtual_to_physical(
        &self,
        virtual_address: VirtualAddress<1>,
    ) -> Option<PhysicalAddress<1>> {
        let vpn_1 = virtual_address.virtual_page_number(1);

        #[allow(clippy::option_if_let_else)]
        if let Some(next_table) = self.get_table(vpn_1) {
            next_table.virtual_to_physical(virtual_address)
        } else {
            self.get_physical_mapping(vpn_1).map(|mapping| {
                PhysicalAddress::const_new(
                    mapping.inner() | virtual_address.page_offset(MAPPING_2MIB),
                )
            })
        }
    }

    /// Add a mapping from a virtual address to a physical address
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a page in memory.
    pub unsafe fn add_mapping(
        &self,
        virtual_address: VirtualAddress<MAPPING_2MIB>,
        physical_address: PhysicalAddress<MAPPING_2MIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        let vpn_1 = virtual_address.virtual_page_number(1);

        // Construct the new entry
        let entry = PageTableEntry::<MAPPING_2MIB>::construct_mapping(
            physical_address,
            permissions,
            user,
            false,
        );

        debug!(
            "Adding Mapping For 2MiB from {:#x} to {:#x} with permissions {:?} and user: {}",
            virtual_address.inner(),
            physical_address.inner(),
            permissions,
            user
        );

        self.set(vpn_1, entry);
    }

    #[allow(clippy::too_many_lines)]
    /// Add a new range mapping
    ///
    /// # Safety
    ///
    /// The mapping constructed must be acceptable to the supervisor or userspace interacting with it.
    ///
    /// # Panics
    ///
    /// This function will panic if the safety contract has been violated
    pub unsafe fn add_range_mapping(
        &self,
        start_physical: PhysicalAddress<MAPPING_4KIB>,
        start_virtual: VirtualAddress<MAPPING_4KIB>,
        length: MemoryUnit<MAPPING_4KIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        trace!(
            "add_range_mapping2({:x?}, {:x?}, {} Pages, {:?}, {})",
            start_physical,
            start_virtual,
            length.raw(),
            permissions,
            user
        );

        // If the relative offset from a 2MiB boundary is the same for both pointers, we can potentially map a full 2MiB
        // page.
        if length.raw_bytes() >= MAPPING_2MIB
            && start_physical.inner() % MAPPING_2MIB == start_virtual.inner() % MAPPING_2MIB
        {
            let next_physical = start_physical.round_up::<MAPPING_2MIB>();
            let next_virtual = start_virtual.round_up::<MAPPING_2MIB>();
            let rounding_distance = next_physical.inner() - start_physical.inner();

            debug!("{:x?}, {:x?}", next_physical, next_virtual);
            debug!("{:x?}", rounding_distance);

            if rounding_distance + MAPPING_2MIB <= length.raw_bytes() {
                let number_of_large_pages = (length.raw_bytes() - rounding_distance) / MAPPING_2MIB;
                for i in 0..number_of_large_pages {
                    self.add_mapping(next_virtual.add(i), next_physical.add(i), permissions, user);
                }

                let length_after_large_pages: MemoryUnit<MAPPING_4KIB> = MemoryUnit::new(
                    (length.raw_bytes() - number_of_large_pages * MAPPING_2MIB - rounding_distance)
                        / MAPPING_4KIB,
                );

                // trace!("Left Mapping");
                if rounding_distance / MAPPING_4KIB != 0 {
                    self.add_range_mapping(
                        start_physical,
                        start_virtual,
                        MemoryUnit::new(rounding_distance / MAPPING_4KIB),
                        permissions,
                        user,
                    );
                }

                // trace!("Left Done");

                // trace!("Right Mapping");
                if length_after_large_pages.raw() != 0 {
                    self.add_range_mapping(
                        next_physical.add(number_of_large_pages).drop_alignment(),
                        next_virtual.add(number_of_large_pages).drop_alignment(),
                        length_after_large_pages,
                        permissions,
                        user,
                    );
                }

                // trace!("Right Done");
                return;
            }
        }

        // If we get here, we are assigning mappings that for some reason are unable to be implemented using 2MiB
        // mappings anymore. Thus, we want to add 4KiB mappings to sub tables at this point.

        // We can start by checking if we can contain the entire range we are mapping into a single Level 0 page table.
        let last_virtual = start_virtual.add(length.raw() - 1);
        if start_virtual.virtual_page_number(1) == last_virtual.virtual_page_number(1) {
            // If so, we are able to hand this task off to the Level 0 page table.
            let level_1_index = start_virtual.virtual_page_number(1);

            // However, first, we need to check if a Level 0 table has already been initialized.
            self.get_table(level_1_index)
                .map_or_else(
                    || {
                        let entry = {
                            // Otherwise, we need to allocate a new Level 0 table.
                            let level_0_table =
                                Allocator::alloc(PageTable::<MAPPING_4KIB, Allocator>::new());

                            // Make a page table entry for it
                            PageTableEntry::<MAPPING_2MIB>::construct_table_mapping(
                                core::ptr::from_mut(level_0_table),
                            )
                        };

                        // trace!("New Entry: {:016x?}", entry);

                        let before_update = self.get(level_1_index).unwrap();

                        // Now that we have the new entry we can add it to the table, however, there is a risk that another
                        // thread has inserted a table underneath us. If this has occurred, we will use their table, however
                        // this means we need to dispose of our new table.
                        let table = match self.inner[level_1_index].compare_exchange(
                            before_update,
                            entry,
                            atomic::Ordering::SeqCst,
                            atomic::Ordering::SeqCst,
                        ) {
                            Ok(_) => {
                                // trace!("OK!");
                                entry
                            }
                            Err(old_table) => {
                                trace!("Err!");
                                Allocator::free(unsafe {
                                    entry
                                        .table_pointer::<Allocator>()
                                        .expect("Table Pointer")
                                        .as_mut()
                                        .expect("Pointer NonNull")
                                });
                                old_table
                            }
                        };

                        // debug!("Selected Table:");
                        // debug!("Pointer: {:#x}", table.pointer());
                        // debug!("Flags: {:x?}", table.flags());
                        // debug!("Flags: {:x?}", table.valid());

                        table
                            .table_pointer()
                            .expect("Table Pointer")
                            .as_ref()
                            .expect("Pointer NonNull")
                    },
                    |table| table,
                )
                .add_range_mapping(start_physical, start_virtual, length, permissions, user);
            return;
        }

        // Finally, if we make it here, we have an allocation that spans multiple Level 0 tables. We are going to walk
        // along this space, and individually request allocations for each of the Level 0 tables.
        let mut walking_start = start_virtual;
        while walking_start.inner() < last_virtual.inner() {
            let next_up = walking_start
                .round_up::<MAPPING_2MIB>()
                .drop_alignment::<MAPPING_4KIB>();

            let end = if next_up.inner() < last_virtual.inner() {
                next_up
            } else {
                last_virtual
            };

            let inner_length = MemoryUnit::<MAPPING_4KIB>::new(
                (end.inner() - walking_start.inner()) / MAPPING_4KIB + 1,
            );

            let inner_offset = (walking_start.inner() - start_virtual.inner()) / MAPPING_4KIB;

            self.add_range_mapping(
                start_physical.add(inner_offset),
                start_virtual.add(inner_offset),
                inner_length,
                permissions,
                user,
            );

            walking_start = end;
        }
    }
}

impl<Allocator: PageAllocator> PageTable<MAPPING_1GIB, Allocator> {
    /// Access the page table mapping within the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_table_mapping(
        &self,
        index: usize,
    ) -> Option<*mut PageTable<MAPPING_2MIB, Allocator>> {
        self.get(index)?.table_pointer()
    }

    /// Access the mapping within the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_mapping(
        &self,
        index: usize,
    ) -> Option<Either<PhysicalAddress<MAPPING_1GIB>, *mut PageTable<MAPPING_2MIB, Allocator>>>
    {
        let entry = self.get(index)?;
        if entry.valid() {
            if entry.flags() == Some(MemoryFlags::PointerToNextLevel) {
                Some(Either::Right(entry.table_pointer()?))
            } else {
                Some(Either::Left(entry.mapping_pointer()?))
            }
        } else {
            None
        }
    }

    /// Access the page table mapped in the `index`th [`PageTableEntry`] in the [`PageTable`]
    pub fn get_table(&self, index: usize) -> Option<&PageTable<MAPPING_2MIB, Allocator>> {
        // SAFETY: The `PageTableEntry` safety invariants give that we have a valid pointer to a `PageTable`, and having
        //     a shared reference to this `PageTable` gives that no mutable references are taken out on the pointed to
        //     table.
        unsafe { self.get_table_mapping(index)?.as_ref() }
    }

    /// Map a virtual address to a physical address
    pub fn virtual_to_physical(
        &self,
        virtual_address: VirtualAddress<1>,
    ) -> Option<PhysicalAddress<1>> {
        let vpn_2 = virtual_address.virtual_page_number(2);

        #[allow(clippy::option_if_let_else)]
        if let Some(next_table) = self.get_table(vpn_2) {
            next_table.virtual_to_physical(virtual_address)
        } else {
            self.get_physical_mapping(vpn_2).map(|mapping| {
                PhysicalAddress::const_new(
                    mapping.inner() | virtual_address.page_offset(MAPPING_1GIB),
                )
            })
        }
    }

    /// Add a mapping from a virtual address to a physical address
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a page in memory.
    pub unsafe fn add_mapping(
        &self,
        virtual_address: VirtualAddress<MAPPING_1GIB>,
        physical_address: PhysicalAddress<MAPPING_1GIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        let vpn_2 = virtual_address.virtual_page_number(2);

        // Construct the new entry
        let entry = PageTableEntry::<MAPPING_1GIB>::construct_mapping(
            physical_address,
            permissions,
            user,
            false,
        );

        self.set(vpn_2, entry);
    }

    #[allow(clippy::too_many_lines)]
    /// Add a new range mapping
    ///
    /// # Safety
    ///
    /// The mapping constructed must be acceptable to the supervisor or userspace interacting with it.
    ///
    /// # Panics
    ///
    /// This function will panic if the safety contract has been violated
    pub unsafe fn add_range_mapping(
        &self,
        start_physical: PhysicalAddress<MAPPING_4KIB>,
        start_virtual: VirtualAddress<MAPPING_4KIB>,
        length: MemoryUnit<MAPPING_4KIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        trace!(
            "add_range_mapping4({:x?}, {:x?}, {} Pages, {:?}, {})",
            start_physical,
            start_virtual,
            length.raw(),
            permissions,
            user
        );

        // If the relative offset from a 1GiB boundary is the same for both pointers, we can potentially map a full 1GiB
        // page.
        if length.raw_bytes() >= MAPPING_1GIB
            && start_physical.inner() % MAPPING_1GIB == start_virtual.inner() % MAPPING_1GIB
        {
            let next_physical = start_physical.round_up::<MAPPING_1GIB>();
            let next_virtual = start_virtual.round_up::<MAPPING_1GIB>();
            let rounding_distance = next_physical.inner() - start_physical.inner();

            debug!("{:x?}, {:x?}", next_physical, next_virtual);
            debug!("{:x?}", rounding_distance);

            if rounding_distance + MAPPING_1GIB <= length.raw_bytes() {
                let number_of_large_pages = (length.raw_bytes() - rounding_distance) / MAPPING_1GIB;
                for i in 0..number_of_large_pages {
                    self.add_mapping(next_virtual.add(i), next_physical.add(i), permissions, user);
                }

                let length_after_large_pages: MemoryUnit<MAPPING_4KIB> = MemoryUnit::new(
                    (length.raw_bytes() - number_of_large_pages * MAPPING_1GIB - rounding_distance)
                        / MAPPING_4KIB,
                );

                // trace!("Left Mapping");
                if rounding_distance / MAPPING_4KIB != 0 {
                    self.add_range_mapping(
                        start_physical,
                        start_virtual,
                        MemoryUnit::new(rounding_distance / MAPPING_4KIB),
                        permissions,
                        user,
                    );
                }

                // trace!("Left Done");

                // trace!("Right Mapping");
                if length_after_large_pages.raw() != 0 {
                    self.add_range_mapping(
                        next_physical.add(number_of_large_pages).drop_alignment(),
                        next_virtual.add(number_of_large_pages).drop_alignment(),
                        length_after_large_pages,
                        permissions,
                        user,
                    );
                }

                // trace!("Right Done");
                return;
            }
        }

        // If we get here, we are assigning mappings that for some reason are unable to be implemented using 1GiB
        // mappings anymore. Thus, we want to add 2MiB mappings to sub tables at this point.

        // We can start by checking if we can contain the entire range we are mapping into a single Level 1 page table.
        let last_virtual = start_virtual.add(length.raw() - 1);
        if start_virtual.virtual_page_number(2) == last_virtual.virtual_page_number(2) {
            // If so, we are able to hand this task off to the Level 1 page table.
            let level_2_index = start_virtual.virtual_page_number(2);

            // However, first, we need to check if a Level 0 table has already been initialized.
            self.get_table(level_2_index)
                .map_or_else(
                    || {
                        let entry = {
                            // Otherwise, we need to allocate a new Level 1 table.
                            let level_1_table =
                                Allocator::alloc(PageTable::<MAPPING_2MIB, Allocator>::new());

                            // Make a page table entry for it
                            PageTableEntry::<MAPPING_1GIB>::construct_table_mapping(
                                core::ptr::from_mut(level_1_table),
                            )
                        };

                        trace!("New Entry: {:016x?}", entry);

                        let before_update = self.get(level_2_index).unwrap();

                        // Now that we have the new entry we can add it to the table, however, there is a risk that another
                        // thread has inserted a table underneath us. If this has occurred, we will use their table, however
                        // this means we need to dispose of our new table.
                        let table = match self.inner[level_2_index].compare_exchange(
                            before_update,
                            entry,
                            atomic::Ordering::SeqCst,
                            atomic::Ordering::SeqCst,
                        ) {
                            Ok(_) => {
                                // trace!("OK!");
                                entry
                            }
                            Err(old_table) => {
                                // trace!("Err!");
                                Allocator::free(unsafe {
                                    entry
                                        .table_pointer::<Allocator>()
                                        .expect("Table Pointer")
                                        .as_mut()
                                        .expect("Pointer NonNull")
                                });
                                old_table
                            }
                        };

                        // debug!("Selected Table:");
                        // debug!("Pointer: {:#x}", table.pointer());
                        // debug!("Flags: {:x?}", table.flags());
                        // debug!("Flags: {:x?}", table.valid());

                        table
                            .table_pointer()
                            .expect("Table Pointer")
                            .as_ref()
                            .expect("Pointer NonNull")
                    },
                    |table| table,
                )
                .add_range_mapping(start_physical, start_virtual, length, permissions, user);
            return;
        }

        // Finally, if we make it here, we have an allocation that spans multiple Level 0 tables. We are going to walk
        // along this space, and individually request allocations for each of the Level 0 tables.
        let mut walking_start = start_virtual;
        while walking_start.inner() < last_virtual.inner() {
            let next_up = walking_start
                .round_up::<MAPPING_1GIB>()
                .drop_alignment::<MAPPING_4KIB>();

            let end = if next_up.inner() < last_virtual.inner() {
                next_up
            } else {
                last_virtual
            };

            let inner_length = MemoryUnit::<MAPPING_4KIB>::new(
                (end.inner() - walking_start.inner()) / MAPPING_4KIB + 1,
            );

            let inner_offset = (walking_start.inner() - start_virtual.inner()) / MAPPING_4KIB;

            self.add_range_mapping(
                start_physical.add(inner_offset),
                start_virtual.add(inner_offset),
                inner_length,
                permissions,
                user,
            );

            walking_start = end;
        }
    }

    /// Identity map a range of addresses. The `start` page is inclusive, and the page that contains the `end` pointer
    /// is included in the mapping.
    ///
    /// # Safety
    ///
    /// The mapping constructed must be acceptable to the supervisor or userspace interacting with it.
    ///
    /// # Panics
    ///
    /// This function will panic if the `start` page is not strictly less than the `end` page.
    pub unsafe fn add_identity_mapping(
        &self,
        start: PhysicalAddress<MAPPING_4KIB>,
        end: PhysicalAddress<1>,
        permissions: MemoryProtectionFlags,
        user: bool,
    ) {
        assert!(start.0 < end.0);

        self.add_range_mapping(
            start,
            start.to_virtual(),
            MemoryUnit::<1>::new(end.0 - start.0).convert(),
            permissions,
            user,
        );
    }
}

impl<const MAPPING_SIZE: usize, Allocator: PageAllocator> core::default::Default
    for PageTable<MAPPING_SIZE, Allocator>
{
    fn default() -> Self {
        Self::new()
    }
}
