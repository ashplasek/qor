pub mod entry;

pub use entry::*;
use qor_core::utils::math::round_up_to;

pub mod table;

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct PhysicalAddress<const ALIGNMENT: usize>(usize);

// SAFETY: The const generic parameter `ALIGNMENT` does not change the in memory representation of `PhysicalAddress`,
//     thus, because all other conditions are satisfied, we can implement `NoUninit` for `PhysicalAddress`
unsafe impl<const ALIGNMENT: usize> bytemuck::NoUninit for PhysicalAddress<ALIGNMENT> {}

impl PhysicalAddress<1> {
    #[inline]
    #[must_use]
    pub const fn const_new(value: usize) -> Self {
        Self(value)
    }
}

impl<const ALIGNMENT: usize> PhysicalAddress<ALIGNMENT> {
    /// Construct a new [`PhysicalAddress`] without checking for alignment.
    ///
    /// # Safety
    ///
    /// The `value` passed must have the proper alignment.
    #[inline]
    #[must_use]
    pub const unsafe fn unchecked_new(value: usize) -> Self {
        Self(value)
    }

    /// Construct a new [`PhysicalAddress`], returning `None` if `value`'s alignment is lesser than `ALIGNMENT`.
    #[inline]
    #[must_use]
    pub const fn new(value: usize) -> Option<Self> {
        if value % ALIGNMENT == 0 {
            Some(Self(value))
        } else {
            None
        }
    }

    /// Construct a new [`PhysicalAddress`], panicking if `value`'s alignment is lesser than `ALIGNMENT`.
    #[inline]
    #[must_use]
    pub const fn panicking_new(value: usize) -> Self {
        if let Some(v) = Self::new(value) {
            v
        } else {
            panic!("Value not aligned");
        }
    }

    /// Get the wrapped value
    #[inline]
    #[must_use]
    pub const fn inner(self) -> usize {
        self.0
    }

    /// Drop the alignment value from the [`PhysicalAddress`]
    #[inline]
    #[must_use]
    pub const fn drop_alignment<const LOWER_ALIGNMENT: usize>(
        self,
    ) -> PhysicalAddress<LOWER_ALIGNMENT> {
        assert!(ALIGNMENT % LOWER_ALIGNMENT == 0);
        PhysicalAddress(self.0)
    }

    /// Offset the [`PhysicalAddress`] by a positive number of `ALIGNMENT`s.
    #[inline]
    #[must_use]
    pub const fn add(self, count: usize) -> Self {
        Self(self.0 + count * ALIGNMENT)
    }

    /// Offset the [`PhysicalAddress`] by a negative number of `ALIGNMENT`s.
    #[inline]
    #[must_use]
    pub const fn sub(self, count: usize) -> Self {
        Self(self.0 - count * ALIGNMENT)
    }

    /// Offset the [`PhysicalAddress`] by a number of `ALIGNMENT`s.
    #[inline]
    #[must_use]
    pub const fn offset(self, count: isize) -> Self {
        if count > 0 {
            self.add(count.unsigned_abs())
        } else if count < 0 {
            self.sub(count.unsigned_abs())
        } else {
            self
        }
    }

    /// Round the [`PhysicalAddress`] up to a `HIGHER_ALIGNMENT`
    #[inline]
    #[must_use]
    pub const fn round_up<const HIGHER_ALIGNMENT: usize>(
        self,
    ) -> PhysicalAddress<HIGHER_ALIGNMENT> {
        // SAFETY: The rounding performed by `round_up_to` results in the proper alignment.
        unsafe { PhysicalAddress::unchecked_new(round_up_to(self.inner(), HIGHER_ALIGNMENT)) }
    }

    /// Convert the physical address to a virtual address by directly copying the address. This is used for identity
    /// mapping.
    #[inline]
    #[must_use]
    pub const fn to_virtual(self) -> VirtualAddress<ALIGNMENT> {
        VirtualAddress(self.0)
    }
}

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct VirtualAddress<const ALIGNMENT: usize>(pub usize);

// SAFETY: The const generic parameter `ALIGNMENT` does not change the in memory representation of `PhysicalAddress`,
//     thus, because all other conditions are satisfied, we can implement `NoUninit` for `PhysicalAddress`
unsafe impl<const ALIGNMENT: usize> bytemuck::NoUninit for VirtualAddress<ALIGNMENT> {}

impl<const ALIGNMENT: usize> VirtualAddress<ALIGNMENT> {
    /// Construct a new [`VirtualAddress`] without checking for alignment.
    ///
    /// # Safety
    ///
    /// The `value` passed must have the proper alignment.
    #[inline]
    #[must_use]
    pub const unsafe fn unchecked_new(value: usize) -> Self {
        Self(value)
    }

    /// Construct a new [`VirtualAddress`], returning `None` if `value`'s alignment is lesser than `ALIGNMENT`.
    #[inline]
    #[must_use]
    pub const fn new(value: usize) -> Option<Self> {
        if value % ALIGNMENT == 0 {
            Some(Self(value))
        } else {
            None
        }
    }

    /// Construct a new [`VirtualAddress`], panicking if `value`'s alignment is lesser than `ALIGNMENT`.
    #[inline]
    #[must_use]
    pub const fn panicking_new(value: usize) -> Self {
        if let Some(v) = Self::new(value) {
            v
        } else {
            panic!("Value not aligned");
        }
    }

    /// Get the wrapped value
    #[inline]
    #[must_use]
    pub const fn inner(self) -> usize {
        self.0
    }

    /// Drop the alignment value from the [`VirtualAddress`]
    #[inline]
    #[must_use]
    pub const fn drop_alignment<const LOWER_ALIGNMENT: usize>(
        self,
    ) -> VirtualAddress<LOWER_ALIGNMENT> {
        assert!(ALIGNMENT % LOWER_ALIGNMENT == 0);
        VirtualAddress(self.0)
    }

    /// Offset the [`VirtualAddress`] by a positive number of `ALIGNMENT`s.
    #[inline]
    #[must_use]
    pub const fn add(self, count: usize) -> Self {
        Self(self.0 + count * ALIGNMENT)
    }

    /// Offset the [`VirtualAddress`] by a negative number of `ALIGNMENT`s.
    #[inline]
    #[must_use]
    pub const fn sub(self, count: usize) -> Self {
        Self(self.0 - count * ALIGNMENT)
    }

    /// Offset the [`VirtualAddress`] by a number of `ALIGNMENT`s.
    #[inline]
    #[must_use]
    pub const fn offset(self, count: isize) -> Self {
        if count > 0 {
            self.add(count.unsigned_abs())
        } else if count < 0 {
            self.sub(count.unsigned_abs())
        } else {
            self
        }
    }

    /// Round the [`VirtualAddress`] up to a `HIGHER_ALIGNMENT`
    #[inline]
    #[must_use]
    pub const fn round_up<const HIGHER_ALIGNMENT: usize>(self) -> VirtualAddress<HIGHER_ALIGNMENT> {
        // SAFETY: The rounding performed by `round_up_to` results in the proper alignment.
        unsafe { VirtualAddress::unchecked_new(round_up_to(self.inner(), HIGHER_ALIGNMENT)) }
    }

    /// Get the offset for a page of a given size from the [`VirtualAddress`]
    #[inline]
    #[must_use]
    pub const fn page_offset(self, page_size: usize) -> usize {
        self.0 & (page_size - 1)
    }

    /// Get the virtual page number `n` portion of the [`VirtualAddress`]
    #[inline]
    #[must_use]
    pub const fn virtual_page_number(self, n: usize) -> usize {
        assert!(n < 3);
        (self.0 >> (12 + 9 * n)) & 0x1ff
    }
}
