use crate::memory::page::PageAllocator;

use super::{
    table::{PageTable, MAPPING_1GIB, MAPPING_2MIB, MAPPING_4KIB},
    PhysicalAddress,
};

const USE_OLD_AD_IMPL: bool = true;

const fn ptr_to_ppn(ptr: usize) -> [usize; 3] {
    assert!(ptr & !((0x1ff << 12) | (0x1ff << 21) | (0x3ff_ffff << 30)) == 0);
    [
        (ptr & (0x1ff << 12)) >> 12,
        (ptr & (0x1ff << 21)) >> 21,
        (ptr & (0x3ff_ffff << 30)) >> 30,
    ]
}

/// Flags used for memory permissions or denoting a pointer to the next [`PageTable`] level in [`PageTableEntry`]s.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MemoryFlags {
    PointerToNextLevel,
    ProtFlag(MemoryProtectionFlags),
}

/// Flags used for memory permissions in [`PageTableEntry`]s.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MemoryProtectionFlags {
    ReadOnly,
    ReadWrite,
    ExecuteOnly,
    ReadExecute,
    ReadWriteExecute,
}

impl MemoryProtectionFlags {
    /// Convert [`MemoryProtectionFlags`] to the bits used in the [`PageTableEntry`] structure.
    #[must_use]
    pub const fn to_bits(self) -> u8 {
        match self {
            Self::ReadOnly => 0b0010,
            Self::ReadWrite => 0b0110,
            Self::ExecuteOnly => 0b1000,
            Self::ReadExecute => 0b1010,
            Self::ReadWriteExecute => 0b1110,
        }
    }

    /// Convert [`MemoryProtectionFlags`] to the bits used in the [`PageTableEntry`] structure for the accessed and
    /// dirty bits.
    #[must_use]
    pub const fn to_ad_bits(self) -> u8 {
        if USE_OLD_AD_IMPL {
            match self {
                Self::ReadOnly | Self::ReadExecute => 0b0100_0000,
                Self::ReadWrite | Self::ReadWriteExecute => 0b1100_0000,
                Self::ExecuteOnly => 0b0000_0000,
            }
        } else {
            unimplemented!()
        }
    }

    /// Convert the flags from the [`PageTableEntry`] to [`MemoryProtectionFlags`].
    #[must_use]
    pub const fn from_bits(bits: u8) -> Option<Self> {
        // Mask off the XWR bits from the [`PagetableEntry`]
        let bits = bits & 0b1110;

        match bits {
            0b0010 => Some(Self::ReadOnly),
            0b0110 => Some(Self::ReadWrite),
            0b1000 => Some(Self::ExecuteOnly),
            0b1010 => Some(Self::ReadExecute),
            0b1110 => Some(Self::ReadWriteExecute),
            _ => None,
        }
    }
}

impl MemoryFlags {
    /// Convert [`MemoryFlags`] to the bits used in the [`PageTableEntry`] structure.
    #[must_use]
    pub const fn to_bits(self) -> u8 {
        match self {
            Self::PointerToNextLevel => 0b0000,
            Self::ProtFlag(flag) => flag.to_bits(),
        }
    }

    /// Convert [`MemoryFlags`] to the bits used in the [`PageTableEntry`] structure for the accessed and
    /// dirty bits.
    #[must_use]
    pub const fn to_ad_bits(self) -> u8 {
        if USE_OLD_AD_IMPL {
            match self {
                Self::PointerToNextLevel => 0b0000_0000,
                Self::ProtFlag(prot_flags) => prot_flags.to_ad_bits(),
            }
        } else {
            unimplemented!()
        }
    }

    /// Convert the flags from the [`PageTableEntry`] to [`MemoryFlags`].
    #[must_use]
    pub const fn from_bits(bits: u8) -> Option<Self> {
        // Mask off the XWR bits from the [`PagetableEntry`]
        let bits = bits & 0b1110;

        if bits == 0 {
            Some(Self::PointerToNextLevel)
        } else if let Some(prot_flags) = MemoryProtectionFlags::from_bits(bits) {
            Some(Self::ProtFlag(prot_flags))
        } else {
            None
        }
    }
}

const VALID_BIT: u64 = 0b0001;
const GLOBAL_BIT: u64 = 0b10_0000;
const USER_BIT: u64 = 0b1_0000;

#[repr(C)]
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// Structure representing an entry into a [`PageTable`]. The `MAPPING_SIZE` const generic parameter is used to denote
/// the size of the memory regions which can be mapped at this level of the [`PageTable`] hierarchy.
///
/// The values `MAPPING_SIZE` can take on are 4Ki, 2Mi, and 1Gi. If a 1Gi [`PageTableEntry`] contains mappings to the
/// "next level", it contains mappings to a [`PageTable`] with `MAPPING_SIZE` 2Mi, similarly for a [`PageTableEntry`]
/// with a `MAPPING_SIZE` of 2Mi and a [`PageTable`] with `MAPPING_SIZE` 4Ki.
///
/// # Safety
///
/// - If the `valid` bit is set, then if `MAPPING_SIZE` is 4Ki, (the smallest page mapping size), then `flags` must
///     *not* be set to `PointerToNextLevel`.
/// - If the `valid` bit is set, and `flags` is set to `PointerToNextLevel`, then `pointer` must be a valid pointer to
///     a [`PageTable`] with the next level.
/// - If the `valid` bit is set, and `flags` is not set to `PointerToNextLevel`, then `pointer` masked to the proper
///     depth must be a valid pointer to a collection of pages with the associated size.
pub struct PageTableEntry<const MAPPING_SIZE: usize>(u64);

// SAFETY: The `MAPPING_SIZE` const generic parameter does not modify the in memory representation of the
//    `PageTableEntry`, therefore, the in memory representation is identical to a `u64`, and therefore is `NoUninit`.
unsafe impl<const MAPPING_SIZE: usize> bytemuck::NoUninit for PageTableEntry<MAPPING_SIZE> {}

impl<const MAPPING_SIZE: usize> PageTableEntry<MAPPING_SIZE> {
    /// Return the valid bit of the [`PageTableEntry`].
    #[must_use]
    #[inline]
    pub const fn valid(self) -> bool {
        self.0 & VALID_BIT > 0
    }

    /// Return the global bit of the [`PageTableEntry`].
    #[must_use]
    #[inline]
    pub const fn global(self) -> bool {
        self.0 & GLOBAL_BIT > 0
    }

    /// Return the user bit of the [`PageTableEntry`].
    #[must_use]
    #[inline]
    pub const fn user(self) -> bool {
        self.0 & USER_BIT > 0
    }

    /// Get the protection flags from the [`PageTableEntry`]
    #[must_use]
    #[inline]
    pub const fn flags(self) -> Option<MemoryFlags> {
        MemoryFlags::from_bits(self.0.to_le_bytes()[0])
    }

    /// Get physical page number 0
    #[must_use]
    #[inline]
    pub const fn physical_page_number_0(self) -> usize {
        ((self.0 >> 10) & 0x1ff) as usize
    }

    /// Get physical page number 1
    #[must_use]
    #[inline]
    pub const fn physical_page_number_1(self) -> usize {
        ((self.0 >> 19) & 0x1ff) as usize
    }

    /// Get physical page number 2
    #[must_use]
    #[inline]
    pub const fn physical_page_number_2(self) -> usize {
        ((self.0 >> 28) & 0x3ff_ffff) as usize
    }

    /// Get the physical page numbers
    #[must_use]
    #[inline]
    pub const fn physical_page_numbers(self) -> [usize; 3] {
        [
            self.physical_page_number_0(),
            self.physical_page_number_1(),
            self.physical_page_number_2(),
        ]
    }

    /// Construct a new [`PageTableEntry`]
    ///
    /// # Safety
    ///
    /// - If `MAPPING_SIZE` is 4Ki, (the smallest page mapping size), then `flags` must *not* be set to
    ///     `PointerToNextLevel`.
    /// - If `flags` is set to `PointerToNextLevel`, then `pointer` must be a valid pointer to
    ///     a [`PageTable`] with the next level.
    /// - If `flags` is not set to `PointerToNextLevel`, then `pointer` masked to the proper
    ///     depth must be a valid pointer to a collection of pages with the associated size.
    #[must_use]
    #[inline]
    pub const unsafe fn unchecked_construct(
        physical_page_numbers: [usize; 3],
        user: bool,
        global: bool,
        flags: MemoryFlags,
    ) -> Self {
        Self(
            (((physical_page_numbers[0] & 0x1ff) << 10) as u64
                | ((physical_page_numbers[1] & 0x1ff) << 19) as u64
                | ((physical_page_numbers[2] & 0x3ff_ffff) << 28) as u64)
                | ((user as u64) << 4)
                | ((global as u64) << 5)
                | flags.to_ad_bits() as u64
                | flags.to_bits() as u64
                | 0b1,
        )
    }

    /// Construct a default, invalid [`PageTableEntry`]
    #[must_use]
    #[inline]
    pub const fn invalid() -> Self {
        // SAFETY: The `valid` bit not being set ensures that none of the safety invariants of [`PageTableEntry`] are
        //     invalidated.
        Self(0)
    }

    /// Get the value of the pointer in the [`PageTableEntry`]
    #[must_use]
    #[inline]
    pub const fn pointer(self) -> usize {
        (self.physical_page_number_0() << 12)
            | (self.physical_page_number_1() << 21)
            | (self.physical_page_number_2() << 30)
    }

    /// Get the physical address for the mapping from the page table entry.
    #[must_use]
    #[inline]
    pub fn mapping_pointer(self) -> Option<PhysicalAddress<MAPPING_SIZE>> {
        if self.valid() && self.flags() != Some(MemoryFlags::PointerToNextLevel) {
            // SAFETY: We have found that the `valid` flag is set and that `flags` is *not* set to `PointerToNextLevel`,
            //     thus `pointer` masked to the proper depth must be a valid pointer to a collection of pages with the
            //     associated size.
            Some(unsafe { PhysicalAddress::unchecked_new(self.pointer() & !(MAPPING_SIZE - 1)) })
        } else {
            None
        }
    }
}

impl PageTableEntry<MAPPING_4KIB> {
    /// Construct a new [`PageTableEntry`] pointing to a memory region of size 4KiB.
    ///
    /// # Panics
    ///
    /// This function will panic if the `memory` pointer is not properly aligned.
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a page in memory.
    #[must_use]
    #[inline]
    pub const unsafe fn construct_mapping(
        memory: PhysicalAddress<MAPPING_4KIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
        global: bool,
    ) -> Self {
        // SAFETY: The valid flag is set, and the flags are not set to `PointerToNextLevel`, thus the safety
        //     requirements on this function are sufficient.
        unsafe {
            Self::unchecked_construct(
                ptr_to_ppn(memory.inner()),
                user,
                global,
                MemoryFlags::ProtFlag(permissions),
            )
        }
    }
}

impl PageTableEntry<MAPPING_2MIB> {
    /// Get a pointer to the mapped [`PageTable`].
    #[must_use]
    #[inline]
    pub fn table_pointer<Allocator: PageAllocator>(
        self,
    ) -> Option<*mut PageTable<MAPPING_4KIB, Allocator>> {
        if self.valid() && self.flags() == Some(MemoryFlags::PointerToNextLevel) {
            // SAFETY: We have found that the `valid` flag is set and that `flags` is set to `PointerToNextLevel`, thus
            //     `pointer` must be a valid pointer to a [`PageTable`] with the next level by the safety invariants of
            //     `PageTableEntry`.
            Some(self.pointer() as *mut PageTable<MAPPING_4KIB, Allocator>)
        } else {
            None
        }
    }

    /// Construct a new [`PageTableEntry`] pointing to a memory region of size 2MiB.
    ///
    /// # Panics
    ///
    /// This function will panic if the `memory` pointer is not properly aligned.
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a megapage in memory.
    #[must_use]
    #[inline]
    pub const unsafe fn construct_mapping(
        memory: PhysicalAddress<MAPPING_2MIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
        global: bool,
    ) -> Self {
        // SAFETY: The valid flag is set, and the flags are not set to `PointerToNextLevel`, thus the safety
        //     requirements on this function are sufficient.
        unsafe {
            Self::unchecked_construct(
                ptr_to_ppn(memory.inner()),
                user,
                global,
                MemoryFlags::ProtFlag(permissions),
            )
        }
    }

    /// Construct a new [`PageTableEntry`] pointing to a [`PageTable`] with `MAPPING_SIZE` of 4Ki.
    ///
    /// # Panics
    ///
    /// This function will panic if the `memory` pointer is not properly aligned.
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a [`PageTable`] with `MAPPING_SIZE` of 4Ki.
    #[must_use]
    #[inline]
    pub unsafe fn construct_table_mapping<Allocator: PageAllocator>(
        table: *mut PageTable<MAPPING_4KIB, Allocator>,
    ) -> Self {
        // SAFETY: The valid flag is set, and the flags are set to `PointerToNextLevel`, thus the safety
        //     requirements on this function are sufficient.
        unsafe {
            Self::unchecked_construct(
                ptr_to_ppn(table.addr()),
                false,
                false,
                MemoryFlags::PointerToNextLevel,
            )
        }
    }
}

impl PageTableEntry<MAPPING_1GIB> {
    /// Get a pointer to the mapped [`PageTable`].
    #[must_use]
    #[inline]
    pub fn table_pointer<Allocator: PageAllocator>(
        self,
    ) -> Option<*mut PageTable<MAPPING_2MIB, Allocator>> {
        if self.valid() && self.flags() == Some(MemoryFlags::PointerToNextLevel) {
            // SAFETY: We have found that the `valid` flag is set and that `flags` is set to `PointerToNextLevel`, thus
            //     `pointer` must be a valid pointer to a [`PageTable`] with the next level by the safety invariants of
            //     `PageTableEntry`.
            Some(self.pointer() as *mut PageTable<MAPPING_2MIB, Allocator>)
        } else {
            None
        }
    }

    /// Construct a new [`PageTableEntry`] pointing to a memory region of size 2MiB.
    ///
    /// # Panics
    ///
    /// This function will panic if the `memory` pointer is not properly aligned.
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a megapage in memory.
    #[must_use]
    #[inline]
    pub const unsafe fn construct_mapping(
        memory: PhysicalAddress<MAPPING_1GIB>,
        permissions: MemoryProtectionFlags,
        user: bool,
        global: bool,
    ) -> Self {
        // SAFETY: The valid flag is set, and the flags are not set to `PointerToNextLevel`, thus the safety
        //     requirements on this function are sufficient.
        unsafe {
            Self::unchecked_construct(
                ptr_to_ppn(memory.inner()),
                user,
                global,
                MemoryFlags::ProtFlag(permissions),
            )
        }
    }

    /// Construct a new [`PageTableEntry`] pointing to a [`PageTable`] with `MAPPING_SIZE` of 2Mi.
    ///
    /// # Panics
    ///
    /// This function will panic if the `memory` pointer is not properly aligned.
    ///
    /// # Safety
    ///
    /// The `memory` pointer must be a valid pointer to a [`PageTable`] with `MAPPING_SIZE` of 2Mi.
    #[must_use]
    #[inline]
    pub unsafe fn construct_table_mapping<Allocator: PageAllocator>(
        table: *mut PageTable<MAPPING_2MIB, Allocator>,
    ) -> Self {
        // SAFETY: The valid flag is set, and the flags are set to `PointerToNextLevel`, thus the safety
        //     requirements on this function are sufficient.
        unsafe {
            Self::unchecked_construct(
                ptr_to_ppn(table.addr()),
                false,
                false,
                MemoryFlags::PointerToNextLevel,
            )
        }
    }
}

#[allow(dead_code)]
mod test {
    use crate::memory::mmu::table::MAPPING_1GIB;

    use super::{ptr_to_ppn, MemoryFlags, MemoryProtectionFlags, PageTableEntry};
    use static_assertions::{const_assert, const_assert_eq};

    const fn equal(a: Option<MemoryFlags>, b: Option<MemoryFlags>) -> bool {
        if let (Some(a), Some(b)) = (a, b) {
            a.to_bits() == b.to_bits()
        } else {
            a.is_none() && b.is_none()
        }
    }

    const_assert_eq!(
        unsafe {
            PageTableEntry::<MAPPING_1GIB>::unchecked_construct(
                ptr_to_ppn(0x394_5193_4000),
                false,
                false,
                MemoryFlags::PointerToNextLevel,
            )
        }
        .pointer(),
        0x394_5193_4000
    );

    const_assert_eq!(
        unsafe {
            PageTableEntry::<MAPPING_1GIB>::unchecked_construct(
                ptr_to_ppn(0xf4_5394_5193_4000),
                false,
                false,
                MemoryFlags::PointerToNextLevel,
            )
        }
        .pointer(),
        0xf4_5394_5193_4000
    );

    const_assert!(!PageTableEntry::<4096>::invalid().valid());

    const SAMPLE_0: PageTableEntry<4096> = unsafe {
        PageTableEntry::unchecked_construct(
            [0x1ff, 0x1ff, 0x3ff_ffff],
            true,
            false,
            MemoryFlags::ProtFlag(MemoryProtectionFlags::ReadWrite),
        )
    };

    const_assert!(SAMPLE_0.valid());
    const_assert!(SAMPLE_0.user());
    const_assert!(!SAMPLE_0.global());
    const_assert!(equal(
        SAMPLE_0.flags(),
        Some(MemoryFlags::ProtFlag(MemoryProtectionFlags::ReadWrite))
    ));
    const_assert_eq!(SAMPLE_0.physical_page_number_0(), 0x1ff);
    const_assert_eq!(SAMPLE_0.physical_page_number_1(), 0x1ff);
    const_assert_eq!(SAMPLE_0.physical_page_number_2(), 0x3ff_ffff);

    const SAMPLE_1: PageTableEntry<4096> = unsafe {
        PageTableEntry::unchecked_construct(
            [0x155, 0xaa, 0x2a5_a55a],
            false,
            true,
            MemoryFlags::ProtFlag(MemoryProtectionFlags::ExecuteOnly),
        )
    };

    const_assert!(SAMPLE_1.valid());
    const_assert!(!SAMPLE_1.user());
    const_assert!(SAMPLE_1.global());
    const_assert!(equal(
        SAMPLE_1.flags(),
        Some(MemoryFlags::ProtFlag(MemoryProtectionFlags::ExecuteOnly))
    ));
    const_assert_eq!(SAMPLE_1.physical_page_number_0(), 0x155);
    const_assert_eq!(SAMPLE_1.physical_page_number_1(), 0xaa);
    const_assert_eq!(SAMPLE_1.physical_page_number_2(), 0x2a5_a55a);
}
