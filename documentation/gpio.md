# Vision Five 2 GPIO

The Vision Five 2 uses a StarFive JH-7110 Processor. This processor can support up to 64 GPIOs which can be mapped to multiple functions [[1]](#ref1) pp. 82 - 84. Each pin has up to three possible functions. A table with these functions is available in [[1]](#ref1) pp. 85 - 89. This table is recreated in [Appendix 1](#a1). Additionally, multiplexing can be applied when pins are in `Function 0` 

# Sources

<a name="ref1"></a>
1. [StarFive JH-7110 Technical Reference Manual](https://doc-en.rvspace.org/JH7110/PDF/JH7110_TRM_StarFive_Preliminary_V2.pdf)

# Appendix 1

Table of pin function options for multiplexing on the StarFive JH-7110 Processor [[1]](#ref1) pp. 85 - 89.

**IOPAD**|**Function 0**|**Function 0 Description**|**Function 1**|**Function 1 Description**|**Function 2**|**Function 2 Description**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
GPIO0|SYS\_GPIO0|The test reset input line for JTAG, Negative| | | | 
GPIO1|SYS\_GPIO1|The test clock input line for JTAG| | | | 
GPIO2|SYS\_GPIO2|The Test Data Input line for JTAG certification (TDI)| | | | 
GPIO3|SYS\_GPIO3|The Test Data Output line for JTAG certification (TDO)| | | | 
GPIO4|SYS\_GPIO4|The Test Mode Selection line for JTAG certification (TMS)| | | | 
GPIO5|SYS\_GPIO5|The Transmit Data line of UART (TXD)| | | | 
GPIO6|SYS\_GPIO6|The Receive Data line of UART (RXD)| | |DVP\_CLK|DVP clock
GPIO7|SYS\_GPIO7|GPIO7|LCD\_CLK|LCD data output|DVP\_VSYNC|DVP vertical sync
GPIO8|SYS\_GPIO8|GPIO8|LCD\_VSYNC|LCD vertical sync|DVP\_HSYNC|DVP horizontal sync
GPIO9|SYS\_GPIO9|The test clock input line for JTAG on HIFI4 (Audio DSP) (TDI)|LCD\_HSYNC|LCD horizontal sync|DVP\_DATA[0]|DCP data line
GPIO10|SYS\_GPIO10|The Test Data Input line for JTAG on HIFI4 (Audio DSP) (TDO)|LCD\_DE|LCD Data Enable mode (DE)|DVP\_DATA[1]|DCP data line
GPIO11|SYS\_GPIO11|The Test Data Output line for JTAG on HIFI4 (Audio DSP) (TDO)|LCD\_DATAOUT[0]|LCD data output|DVP\_DATA[2]|DCP data line
GPIO12|SYS\_GPIO12|The Test Mode Selection for JTAG on HIFI4 (Audio DSP) (TMS)|LCD\_DATAOUT[1]|LCD data output|DVP\_DATA[3]|DCP data line
GPIO13|SYS\_GPIO13|The test reset input line for JTAG on HIFI4 (Audio DSP), Negative|LCD\_DATAOUT[2]|LCD data output|DVP\_DATA[4]|DCP data line
GPIO14|SYS\_GPIO14|GPIO14|LCD\_DATAOUT[3]|LCD data output|DVP\_DATA[5]|DCP data line
GPIO15|SYS\_GPIO15|GPIO15|LCD\_DATAOUT[4]|LCD data output|DVP\_DATA[6]|DCP data line
GPIO16|SYS\_GPIO16|GPIO16|LCD\_DATAOUT[5]|LCD data output|DVP\_DATA[7]|DCP data line
GPIO17|SYS\_GPIO17|GPIO17|LCD\_DATAOUT[6]|LCD data output|DVP\_DATA[8]|DCP data line
GPIO18|SYS\_GPIO18|The Transmit Data line of UART (TXD)|LCD\_DATAOUT[7]|LCD data output|DVP\_DATA[9]|DCP data line
GPIO19|SYS\_GPIO19|The Receive Data line of UART (RXD)|LCD\_DATAOUT[8]|LCD data output|DVP\_DATA[10]|DCP data line
GPIO20|SYS\_GPIO20|GPIO20|LCD\_DATAOUT[9]|LCD data output|DVP\_DATA[11]|DCP data line
GPIO21|SYS\_GPIO21|GPIO21|LCD\_DATAOUT[10]|LCD data output|DVP\_CLK|DVP clock
GPIO22|SYS\_GPIO22|GPIO22|LCD\_DATAOUT[11]|LCD data output|DVP\_VSYNC|DVP vertical sync
GPIO23|SYS\_GPIO23|GPIO23|LCD\_DATAOUT[12]|LCD data output|DVP\_HSYNC|DVP horizontal sync
GPIO24|SYS\_GPIO24|GPIO24|LCD\_DATAOUT[13]|LCD data output|DVP\_DATA[0]|DCP data line
GPIO25|SYS\_GPIO25|GPIO25|LCD\_DATAOUT[14]|LCD data output|DVP\_DATA[1]|DCP data line
GPIO26|SYS\_GPIO26|GPIO26|LCD\_DATAOUT[15]|LCD data output|DVP\_DATA[2]|DCP data line
GPIO27|SYS\_GPIO27|GPIO27|LCD\_DATAOUT[16]|LCD data output|DVP\_DATA[3]|DCP data line
GPIO28|SYS\_GPIO28|GPIO28|LCD\_DATAOUT[17]|LCD data output|DVP\_DATA[4]|DCP data line
GPIO29|SYS\_GPIO29|GPIO29|LCD\_DATAOUT[18]|LCD data output|DVP\_DATA[5]|DCP data line
GPIO30|SYS\_GPIO30|GPIO30|LCD\_DATAOUT[19]|LCD data output|DVP\_DATA[6]|DCP data line
GPIO31|SYS\_GPIO31|GPIO31|LCD\_DATAOUT[20]|LCD data output|DVP\_DATA[7]|DCP data line
GPIO32|SYS\_GPIO32|GPIO32|LCD\_DATAOUT[21]|LCD data output|DVP\_DATA[8]|DCP data line
GPIO33|SYS\_GPIO33|GPIO33|LCD\_DATAOUT[22]|LCD data output|DVP\_DATA[9]|DCP data line
GPIO34|SYS\_GPIO34|GPIO34|LCD\_DATAOUT[23]|LCD data output|DVP\_DATA[10]|DCP data line
GPIO35|SYS\_GPIO35|The reset output line of WDT| | |DVP\_DATA[11]|DCP data line
GPIO36|SYS\_GPIO36|The SCL signal trace of I2C|LCD\_CLK|LCD data output|DVP\_VSYNC|DVP vertical sync
GPIO37|SYS\_GPIO37|The SDA signal trace of I2C|LCD\_VSYNC|LCD vertical sync|DVP\_HSYNC|DVP horizontal sync
GPIO38|SYS\_GPIO38|The SCL signal trace of I2C|LCD\_HSYNC|LCD horizontal sync|DVP\_DATA[0]|DCP data line
GPIO39|SYS\_GPIO39|The SDA signal trace of I2C|LCD\_DE|LCD Data Enable mode (DE)|DVP\_DATA[1]|DCP data line
GPIO40|SYS\_GPIO40|The Receive Data line of UART (RXD)|LCD\_DATAOUT[0]|LCD data output|DVP\_DATA[2]|DCP data line
GPIO41|SYS\_GPIO41|The Transmit Data line of UART (TXD)|LCD\_DATAOUT[1]|LCD data output|DVP\_DATA[3]|DCP data line
GPIO42|SYS\_GPIO42|The modem control Request to Send output line of UART, Negative (RTS)|LCD\_DATAOUT[2]|LCD data output|DVP\_DATA[4]|DCP data line
GPIO43|SYS\_GPIO43|The Clear to Send modem status of UART, Negative (CTS)|LCD\_DATAOUT[3]|LCD data output|DVP\_DATA[5]|DCP data line
GPIO44|SYS\_GPIO44|The Receive Data line of UART (RXD)|LCD\_DATAOUT[4]|LCD data output|DVP\_DATA[6]|DCP data line
GPIO45|SYS\_GPIO45|The Transmit Data line of UART (TXD)|LCD\_DATAOUT[5]|LCD data output|DVP\_DATA[7]|DCP data line
GPIO46|SYS\_GPIO46|The modem control Request to Send output line of UART, Negative (RTS)|LCD\_DATAOUT[6]|LCD data output|DVP\_DATA[8]|DCP data line
GPIO47|SYS\_GPIO47|The Clear to Send modem status of UART, Negative (CTS)|LCD\_DATAOUT[7]|LCD data output|DVP\_DATA[9]|DCP data line
GPIO48|SYS\_GPIO48|The chip select of SPI, Negative|LCD\_DATAOUT[8]|LCD data output|DVP\_DATA[10]|DCP data line
GPIO49|SYS\_GPIO49|The series clock line of SPI|LCD\_DATAOUT[9]|LCD data output|DVP\_DATA[11]|DCP data line
GPIO50|SYS\_GPIO50|The Receive Data line of SPI (RXD)|LCD\_DATAOUT[10]|LCD data output| | 
GPIO51|SYS\_GPIO51|The Transmit Data line of SPI (TXD)|LCD\_DATAOUT[11]|LCD data output| | 
GPIO52|SYS\_GPIO52|The chip select of SPI, Negative|LCD\_DATAOUT[12]|LCD data output| | 
GPIO53|SYS\_GPIO53|The series clock line of SPI|LCD\_DATAOUT[13]|LCD data output| | 
GPIO54|SYS\_GPIO54|The Receive Data line of SPI (RXD)|LCD\_DATAOUT[14]|LCD data output| | 
GPIO55|SYS\_GPIO55|The Transmit Data line of SPI (TXD)|LCD\_DATAOUT[15]|LCD data output| | 
GPIO56|SYS\_GPIO56|The chip select of SPI, Negative|LCD\_DATAOUT[16]|LCD data output| | 
GPIO57|SYS\_GPIO57|The series clock line of SPI|LCD\_DATAOUT[17]|LCD data output| | 
GPIO58|SYS\_GPIO58|The Receive Data line of SPI (RXD)|LCD\_DATAOUT[18]|LCD data output| | 
GPIO59|SYS\_GPIO59|The Transmit Data line of SPI (TXD)|LCD\_DATAOUT[19]|LCD data output| | 
GPIO60|SYS\_GPIO60|The chip select of SPI, Negative|LCD\_DATAOUT[20]|LCD data output| | 
GPIO61|SYS\_GPIO61|The series clock line of SPI|LCD\_DATAOUT[21]|LCD data output| | 
GPIO62|SYS\_GPIO62|The Receive Data line of SPI (RXD)|LCD\_DATAOUT[22]|LCD data output| | 
GPIO63|SYS\_GPIO63|The Transmit Data line of SPI (TXD)|LCD\_DATAOUT[23]|LCD data output| | 
