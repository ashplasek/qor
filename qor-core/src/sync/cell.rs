use bytemuck::NoUninit;
use core::{cell::SyncUnsafeCell, mem::MaybeUninit};

#[repr(u8)]
#[derive(Copy, Clone, NoUninit)]
enum InitializationState {
    Uninitialized,
    InitializationInProgress,
    Initialized,
}

/// A sync implementation of a `OnceCell`
///
/// # Safety
///
/// If `state` is set to `Initialized`, then the contents of `cell` must be a properly initialized `T`.
/// If `state` is set to `InitializationInProgress` or `Initialized`, no new exclusive references can be taken out
///     without an exclusive reference to `OnceSyncCell`.
/// If `state` is set to `Uninitialized`, a single exclusive reference can be taken out, doing so will cause `state` to
///     be updated to `InitializationInProgress` while that reference is alive, and to `Initialized` once that
///     reference's lifetime ends.
#[allow(clippy::module_name_repetitions)]
pub struct OnceSyncCell<T> {
    state: atomic::Atomic<InitializationState>,
    cell: SyncUnsafeCell<MaybeUninit<T>>,
}

impl<T> OnceSyncCell<T> {
    /// Construct a new `OnceSyncCell`, containing an uninitialized value.
    #[must_use]
    pub const fn new() -> Self {
        Self {
            state: atomic::Atomic::new(InitializationState::Uninitialized),
            cell: SyncUnsafeCell::new(MaybeUninit::uninit()),
        }
    }

    /// Get a reference to the `MaybeUninit<T>` from inside the `UnsafeCell`.
    ///
    /// # Safety
    ///
    /// There must not be an exclusive reference to the `cell`'s contents.
    unsafe fn inner(&self) -> &MaybeUninit<T> {
        // Safety:
        // - All guarantees of pointer alignment and validity are handled by `UnsafeCell::get`
        // - Aliasing is prevented because
        self.cell.get().as_ref().unwrap()
    }

    /// Get an exclusive reference to the `MaybeUninit<T>` from inside the `UnsafeCell`.
    ///
    /// # Safety
    ///
    /// No other thread may alias this reference.
    #[allow(clippy::mut_from_ref)] // The safety requirements of this function demand no aliasing.
    unsafe fn inner_mut(&self) -> &mut MaybeUninit<T> {
        // Safety:
        // - All guarantees of pointer alignment and validity are handled by `UnsafeCell::get`
        // - Aliasing is prevented via the safety requirement of this function.
        self.cell.get().as_mut().unwrap()
    }

    /// Try to access the contents of the `cell` via a shared reference. This function will return `None` if the cell is
    /// not yet fully initialized.
    pub fn try_get(&self) -> Option<&T> {
        match self.state.load(atomic::Ordering::Acquire) {
            // Safety: The safety invariants of `OnceSyncCell` give that if `state` is `Initialized`, no exclusive
            //     references may be held to the contents of the `cell`, thus we can call `inner`, and the contents of
            //     `cell` is properly initialized, thus we can safely call `assume_init_ref`.
            InitializationState::Initialized => Some(unsafe { self.inner().assume_init_ref() }),
            _ => None,
        }
    }

    /// Try to initialize the `OnceSyncCell`, on success, the `cell` will be initialized to `object`, and this function
    /// will return `Ok(())`. If it was not possible to initialize the `cell`, the `object` will be returned via an
    /// `Err(T)`.
    ///
    /// A failure means that another thread is either in the process of initializing the `cell` or already initialized
    /// the `cell`.
    ///
    /// # Errors
    ///
    /// This function will return an `Err(T)` if it was not possible to initialize the `cell`. The `T` returned is the
    /// `object` the initialization was attempted with.
    pub fn try_initialize(&self, object: T) -> Result<(), T> {
        // We take a lock on the contents of the `cell` by updating `state` to `InitializationInProgress`, and do not
        // allow initialization if the previous `state` was anything other than `Uninitialized`
        if self
            .state
            .compare_exchange(
                InitializationState::Uninitialized,
                InitializationState::InitializationInProgress,
                atomic::Ordering::AcqRel,
                atomic::Ordering::Acquire,
            )
            .is_err()
        {
            return Err(object);
        }

        // Safety: By taking the lock associated with `state`, we ensure exclusive access to the contents of the `cell`.
        unsafe {
            self.inner_mut().write(object);
        }

        // Finally, once the `cell` has been initialized, the `state` lock is released, and put in `Initialized`. Thus
        // making it possible for shared references to be taken out. Additionally, we uphold the safety invariant of
        // `state` being initialized implying that `cell` contains a properly initialized `T`.
        self.state
            .store(InitializationState::Initialized, atomic::Ordering::Release);

        Ok(())
    }
}

impl<T> core::ops::Drop for OnceSyncCell<T> {
    fn drop(&mut self) {
        match self.state.load(atomic::Ordering::Acquire) {
            InitializationState::Initialized => {
                // Safety: Because we have an exclusive reference to `self`, we can take out an exclusive reference to
                //     the contents of the `cell` while in the `Initialized` `state`, and thus it is safe to call
                //     `inner_mut`. The safety invariants of `OnceSyncCell` give that because `state` is `Initialized`,
                //     the contents of the `cell` must be properly initialized, and thus it is safe to call
                //     `assume_init_drop`.
                unsafe { self.inner_mut().assume_init_drop() };
            }
            InitializationState::InitializationInProgress => unreachable!(),
            InitializationState::Uninitialized => {}
        }
    }
}

impl<T> core::default::Default for OnceSyncCell<T> {
    fn default() -> Self {
        Self::new()
    }
}
