#[inline]
#[must_use]
pub const fn round_up_to(value: usize, rounding: usize) -> usize {
    if value % rounding != 0 {
        value + (rounding - (value % rounding))
    } else {
        value
    }
}

#[cfg(feature = "std")]
#[cfg(test)]
mod test {
    use crate::utils::math::round_up_to;

    fn rounding_up_for(rounding_amount: usize) {
        assert_eq!(round_up_to(0, rounding_amount), 0);

        for i in 0..10 {
            for j in 1..rounding_amount {
                assert_eq!(
                    round_up_to(i * rounding_amount + j, rounding_amount),
                    (i + 1) * rounding_amount
                );
            }
        }
    }

    #[test]
    fn rounding_up() {
        rounding_up_for(1);
        rounding_up_for(2);
        rounding_up_for(3);
        rounding_up_for(4);
        rounding_up_for(5);
        rounding_up_for(8);
        rounding_up_for(32);

        assert_eq!(round_up_to(0x4315, 0x1000), 0x5000);
        assert_eq!(round_up_to(0x4fff, 0x1000), 0x5000);
        assert_eq!(round_up_to(0x4000, 0x1000), 0x4000);
        assert_eq!(round_up_to(0xffff, 0x1000), 0x10000);
        assert_eq!(round_up_to(0x4315, 0x20), 0x4320);
    }
}
