/// Collection of locks stored together in a bitmap
#[allow(clippy::module_name_repetitions)]
pub struct BitmapLock<'a> {
    bitmap: &'a [core::sync::atomic::AtomicU64],
    length: usize,
}

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BitmapError {
    RangeOutOfBounds {
        start: usize,
        end: usize,
        length: usize,
    },
    UnableToAllocate {
        length: usize,
    },
}

impl<'a> BitmapLock<'a> {
    /// Construct a new, empty `BitmapLock`
    #[must_use]
    pub const fn new() -> Self {
        Self {
            bitmap: &[],
            length: 0,
        }
    }

    /// Construct a new `BitmapLock` from a buffer of `AtomicU64`s and a length
    pub const fn from_data(bitmap: &'a [core::sync::atomic::AtomicU64], length: usize) -> Self {
        let use_length = if bitmap.len() * 64 <= length {
            bitmap.len() * 64
        } else {
            length
        };

        Self {
            bitmap,
            length: use_length,
        }
    }

    fn try_set_in_entry(&self, entry_index: usize, mask: u64) -> bool {
        let read = self.bitmap[entry_index].fetch_or(mask, core::sync::atomic::Ordering::AcqRel);

        // If there were bits set that overlap our write, we need to leave those bits intact, and remove the excess bits
        if read & mask != 0 {
            let excess = mask & (!read);
            self.clear_in_entry(entry_index, excess);
        }

        // If none of the bits which are set in the mask were set when we wrote to the entry, we were the ones to
        // acquire those bits.
        read & mask == 0
    }

    /// Attempts to set a sequence of `count` bits starting at *bit* index `index`. Note that any index returned will
    /// be less than `length`.
    ///
    /// # Errors
    ///
    /// This function will return an error if `count` bits after `index` does not fit within the bitmap.
    pub fn try_set(&self, index: usize, count: usize) -> Result<bool, BitmapError> {
        if index + count > self.length {
            Err(BitmapError::RangeOutOfBounds {
                start: index,
                end: index + count,
                length: self.length,
            })
        } else {
            // Compute the entry index
            let entry_index_start = index / 64;
            let offset_into_entry = index % 64;

            let bit_count = count.min(64 - offset_into_entry);

            #[allow(clippy::cast_possible_truncation)] // `bit_count` is less than or equal to 64
            let this_mask = if bit_count < 64 {
                (1u64.wrapping_shl(bit_count as u32)).wrapping_sub(1) << offset_into_entry
            } else {
                u64::MAX
            };

            if self.try_set_in_entry(entry_index_start, this_mask) {
                let next_count = count - bit_count;
                if next_count > 0 {
                    if self.try_set(index + bit_count, count - bit_count)? {
                        Ok(true)
                    } else {
                        // If a later set failed, we need to clear the bits we have already set
                        self.clear(index, bit_count)?;

                        Ok(false)
                    }
                } else {
                    Ok(true)
                }
            } else {
                Ok(false)
            }
        }
    }

    fn clear_in_entry(&self, entry_index: usize, mask: u64) {
        let result =
            self.bitmap[entry_index].fetch_and(!mask, core::sync::atomic::Ordering::AcqRel);

        if result & mask != mask {
            crate::warn!("Attempted to clear bits with mask {:x} at entry {} in bitmap lock, some bits were already cleared");
        }
    }

    /// Clears a sequence of `count` bits starting at *bit* index `index`.
    ///
    /// # Errors
    ///
    /// This function will return an error if `count` bits after index does not fit within the bitmap.
    pub fn clear(&self, index: usize, count: usize) -> Result<(), BitmapError> {
        if index + count > self.length {
            Err(BitmapError::RangeOutOfBounds {
                start: index,
                end: index + count,
                length: self.length,
            })
        } else {
            // Compute the entry index
            let entry_index_start = index / 64;
            let offset_into_entry = index % 64;

            let bit_count = count.min(64 - offset_into_entry);

            #[allow(clippy::cast_possible_truncation)] // `bit_count` is less than or equal to 64
            let this_mask = if bit_count < 64 {
                (1u64.wrapping_shl(bit_count as u32)).wrapping_sub(1) << offset_into_entry
            } else {
                u64::MAX
            };

            self.clear_in_entry(entry_index_start, this_mask);
            let next_count = count - bit_count;
            if next_count > 0 {
                self.clear(index + bit_count, count - bit_count)?;
            }

            Ok(())
        }
    }

    /// Request a sequence of `count` bits to be locked, returning the *bit* index of the first lock if one is found.
    /// The index returned will be less than `length`.
    ///
    /// # Errors
    ///
    /// This function will return an error if it was unable to allocate `count` bits in the bitmap.
    pub fn reserve_sequence(&self, count: usize) -> Result<usize, BitmapError> {
        // Loop over the entries in the table
        for entry_index in 0..self.bitmap.len() {
            let mut mask_off_impossible_indexes =
                self.bitmap[entry_index].load(core::sync::atomic::Ordering::Relaxed);

            while mask_off_impossible_indexes != u64::MAX {
                let bit_index = mask_off_impossible_indexes.trailing_ones();
                let bit_offset = bit_index as usize;

                let index = entry_index * 64 + bit_offset;

                // Note that the index returned must be less than `length`.
                match self.try_set(index, count) {
                    Ok(true) => {
                        return Ok(index);
                    }
                    Err(BitmapError::RangeOutOfBounds { .. }) => {
                        break;
                    }
                    _ => {}
                }

                mask_off_impossible_indexes |= 1 << bit_index;
            }
        }

        Err(BitmapError::UnableToAllocate { length: count })
    }
}

impl<'a> core::default::Default for BitmapLock<'a> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(feature = "std")]
#[cfg(test)]
mod test {
    use core::{cell::UnsafeCell, sync::atomic::AtomicU64};

    use alloc::sync::Arc;

    use super::BitmapLock;

    struct Wrapper(UnsafeCell<u64>);

    unsafe impl Sync for Wrapper {}

    #[cfg(not(miri))]
    const ITERATIONS: u64 = 1000;

    #[cfg(miri)]
    const ITERATIONS: u64 = 10;

    #[test]
    pub fn lock_single() {
        // Then we construct a `BitmapLock` to guard these values
        static ATOMICS: [AtomicU64; 2] = [const { AtomicU64::new(0) }; 2];

        // We set up an array of 10 integers that we store in `UnsafeCells` to allow us to potentially invoke Undefined
        // Behavior if our locking isn't correct.
        let values = Arc::new([const { Wrapper(UnsafeCell::new(0)) }; 10]);

        let locks = Arc::new(BitmapLock::from_data(&ATOMICS, 10));

        // Next, 20 Threads are spun up, so we have two threads racing to modify each value
        let mut threads = alloc::vec::Vec::new();
        for i in 0..20 {
            let index = i % 10;

            let locks = locks.clone();
            let values = values.clone();

            threads.push(std::thread::spawn(move || {
                // Each thread loops for `ITERATIONS` cycles
                for _ in 0..ITERATIONS {
                    // Locks the value in the `BitmapLock`
                    while !locks.try_set(index, 1).unwrap() {}

                    // Does the unsafe addition
                    unsafe {
                        *(values[index].0.get().as_mut().unwrap()) += 1;
                    }

                    // And releases the lock
                    locks.clear(index, 1).unwrap();
                }
            }));
        }

        // We wait for all of the threads
        for t in threads {
            t.join().unwrap();
        }

        // And finally verify that all of the values got to the expected sum
        for v in &*values {
            assert_eq!(unsafe { *v.0.get().as_ref().unwrap() }, 2 * ITERATIONS);
        }
    }

    #[test]
    pub fn lock_multiple() {
        // Then we construct a `BitmapLock` to guard these values
        static ATOMICS: [AtomicU64; 2] = [const { AtomicU64::new(0) }; 2];

        // We set up an array of 32 integers that we store in `UnsafeCells` to allow us to potentially invoke Undefined
        // Behavior if our locking isn't correct.
        let values = Arc::new([const { Wrapper(UnsafeCell::new(0)) }; 32]);

        let locks = Arc::new(BitmapLock::from_data(&ATOMICS, 32));

        // Next, 10 Threads are spun up, so we have two threads racing to modify each value
        let mut threads = alloc::vec::Vec::new();
        for i in 0..10 {
            let index = i % 5;
            let step_size = 1 << index;

            let locks = locks.clone();
            let values = values.clone();

            threads.push(std::thread::spawn(move || {
                // Each thread loops for `ITERATIONS` cycles
                for _ in 0..ITERATIONS {
                    for i in 0..(32 / step_size) {
                        // Locks the values in the `BitmapLock`
                        while !locks.try_set(i * step_size, step_size).unwrap() {}

                        // Does the unsafe addition to each value
                        for index in i * step_size..(i + 1) * step_size {
                            unsafe {
                                *(values[index].0.get().as_mut().unwrap()) += 1;
                            }
                        }

                        // And releases the lock
                        locks.clear(i * step_size, step_size).unwrap();
                    }
                }
            }));
        }

        // We wait for all of the threads
        for t in threads {
            t.join().unwrap();
        }

        // And finally verify that all of the values got to the expected sum
        for v in &*values {
            assert_eq!(unsafe { *v.0.get().as_ref().unwrap() }, 10 * ITERATIONS);
        }
    }

    #[test]
    pub fn lock_multiple_big() {
        // Then we construct a `BitmapLock` to guard these values
        static ATOMICS: [AtomicU64; 8] = [const { AtomicU64::new(0) }; 8];

        const THIS_ITERATIONS: u64 = ITERATIONS / 16;

        // We set up an array of 512 integers that we store in `UnsafeCells` to allow us to potentially invoke Undefined
        // Behavior if our locking isn't correct.
        let values = Arc::new([const { Wrapper(UnsafeCell::new(0)) }; 512]);

        let locks = Arc::new(BitmapLock::from_data(&ATOMICS, 512));

        // Next, 16 Threads are spun up, so we have two threads racing to modify each value
        let mut threads = alloc::vec::Vec::new();
        for i in 0..16 {
            let index = i % 8;
            let step_size = 1 << index;

            let locks = locks.clone();
            let values = values.clone();

            threads.push(std::thread::spawn(move || {
                // Each thread loops for `THIS_ITERATIONS` cycles
                for _ in 0..THIS_ITERATIONS {
                    for i in 0..(512 / step_size) {
                        // Locks the values in the `BitmapLock`
                        while !locks.try_set(i * step_size, step_size).unwrap() {}

                        // Does the unsafe addition to each value
                        for index in i * step_size..(i + 1) * step_size {
                            unsafe {
                                *(values[index].0.get().as_mut().unwrap()) += 1;
                            }
                        }

                        // And releases the lock
                        locks.clear(i * step_size, step_size).unwrap();
                    }
                }
            }));
        }

        // We wait for all of the threads
        for t in threads {
            t.join().unwrap();
        }

        // And finally verify that all of the values got to the expected sum
        for v in &*values {
            assert_eq!(
                unsafe { *v.0.get().as_ref().unwrap() },
                16 * THIS_ITERATIONS
            );
        }
    }
}
