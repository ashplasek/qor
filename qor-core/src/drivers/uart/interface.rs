use crate::sync::{Mutex, MutexGuard};

use super::{DataMode, Interrupts};

/// Generic interface for a UART Driver
#[allow(clippy::module_name_repetitions)]
pub trait UARTInterface {
    /// Set the `DataMode`
    fn set_data_mode(&self, data: DataMode);

    /// Set the `Interrupts` state
    fn set_interrupt_enables(&self, interrupts: Interrupts);

    /// Set the `baud_rate` (requires updating the `DataMode` as well)
    fn set_baud_rate(&self, data: DataMode, baud_rate: u32);

    /// Send a byte over the uart port
    fn send_byte_sync(&self, byte: u8);

    /// Attempt to receive a byte from the uart port
    #[must_use]
    fn receive_byte(&self) -> Option<u8>;

    /// Construct a second handle pointing to a `UARTDriver`.
    ///
    /// # Safety
    ///
    /// Calling this function abandons any guarantees of exclusivity in UART operations.
    #[must_use]
    unsafe fn duplicate(&self) -> Self;
}

pub struct GlobalUART<Driver: UARTInterface> {
    driver: Driver,
    locked_driver: Mutex<Driver>,
}

impl<Driver: UARTInterface> GlobalUART<Driver> {
    /// Construct a new `GlobalUART` lock used for sharing access to the wrapped driver semi-exclusively.
    #[must_use]
    pub fn new(driver: Driver) -> Self {
        Self {
            driver: unsafe { driver.duplicate() },
            locked_driver: Mutex::new(driver),
        }
    }

    /// Attempt to lock the safe handle to the `Driver`.
    pub fn attempt_lock(&self) -> Option<MutexGuard<'_, Driver>> {
        self.locked_driver.attempt_lock()
    }

    /// Lock the safe handle using a spin lock
    pub fn spin_lock(&self) -> MutexGuard<'_, Driver> {
        self.locked_driver.spin_lock()
    }

    /// Gain access to the unsafe handle to the `Driver`
    ///
    /// # Safety
    ///
    /// Calling this function abandons the exclusivity expected by locking the `Driver`.
    #[must_use]
    pub const unsafe fn force_access(&self) -> DriverRef<'_, Driver> {
        DriverRef {
            reference: &self.driver,
        }
    }
}

pub struct DriverRef<'a, T: UARTInterface> {
    reference: &'a T,
}

impl<'a, T: UARTInterface> core::ops::Deref for DriverRef<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.reference
    }
}

impl<'a, T: UARTInterface> core::fmt::Write for DriverRef<'a, T> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for b in s.bytes() {
            self.send_byte_sync(b);
        }

        Ok(())
    }
}
