#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Parity {
    None,
    Odd,
    Even,
    AlwaysZero,
    AlwaysOne,
}

impl Parity {
    #[must_use]
    pub const fn stick_bit(&self) -> bool {
        matches!(self, Self::AlwaysOne | Self::AlwaysZero)
    }

    #[must_use]
    pub const fn parity_type(&self) -> bool {
        matches!(self, Self::Even | Self::AlwaysZero)
    }

    #[must_use]
    pub const fn parity_enable(&self) -> bool {
        !matches!(self, Self::None)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DataLength {
    FiveBits,
    SixBits,
    SevenBits,
    EightBits,
}

impl DataLength {
    #[must_use]
    pub const fn value(&self) -> u8 {
        match self {
            Self::FiveBits => 0,
            Self::SixBits => 1,
            Self::SevenBits => 2,
            Self::EightBits => 3,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct DataMode {
    send_break: bool,
    extra_stop_bits: bool,
    length: DataLength,
    parity: Parity,
}

impl DataMode {
    #[must_use]
    pub const fn new(
        send_break: bool,
        extra_stop_bits: bool,
        length: DataLength,
        parity: Parity,
    ) -> Self {
        Self {
            send_break,
            extra_stop_bits,
            length,
            parity,
        }
    }

    #[must_use]
    pub const fn compute_lcr(&self) -> u8 {
        self.length.value()
            | ((self.extra_stop_bits as u8) << 2)
            | ((self.parity.parity_enable() as u8) << 3)
            | ((self.parity.parity_type() as u8) << 4)
            | ((self.parity.stick_bit() as u8) << 5)
            | ((self.send_break as u8) << 7)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Default)]
pub struct Interrupts {
    pub receive: bool,
    pub transmit_empty: bool,
}

impl Interrupts {
    #[must_use]
    pub const fn receive() -> Self {
        Self {
            receive: true,
            transmit_empty: false,
        }
    }

    #[must_use]
    pub const fn transmit_empty() -> Self {
        Self {
            receive: false,
            transmit_empty: true,
        }
    }

    #[must_use]
    pub const fn enable_receive(mut self) -> Self {
        self.receive = true;
        self
    }

    #[must_use]
    pub const fn disable_receive(mut self) -> Self {
        self.receive = false;
        self
    }

    #[must_use]
    pub const fn enable_transmit_empty(mut self) -> Self {
        self.transmit_empty = true;
        self
    }

    #[must_use]
    pub const fn disable_transmit_empty(mut self) -> Self {
        self.transmit_empty = false;
        self
    }

    #[must_use]
    pub const fn value(&self) -> u8 {
        self.receive as u8 | ((self.transmit_empty as u8) << 1)
    }
}
