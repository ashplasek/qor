#![no_std]
#![feature(pointer_is_aligned_to)] // Required for allocator tests
#![feature(ptr_sub_ptr)] // Required for allocator pointer arithmetic
#![feature(strict_provenance)] // Required for supporting provenance in Miri tests
#![feature(strict_provenance_atomic_ptr)] // Required for full use of atomic pointers
#![feature(sync_unsafe_cell)] // Required for sound mutable statics

#[cfg(feature = "std")]
#[macro_use]
extern crate std;

#[cfg(feature = "alloc")]
extern crate alloc;

pub mod drivers;
pub mod logging;
pub mod memory;
pub mod sync;
pub mod utils;
